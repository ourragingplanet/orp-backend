<?php

use Phaza\LaravelPostgis\Geometries\LineString;
use Phaza\LaravelPostgis\Geometries\Point;
use Phaza\LaravelPostgis\Geometries\Polygon;
use GeoJson\GeoJson;
use Illuminate\Database\Seeder;
use Cocur\Slugify\Slugify;
use App\Models\CachedDataServerFeature;
use App\Models\CachedDataServerFeatureSet;
use App\Models\Features\FeatureSet;
use App\Models\Features\District;

class IndustrialHeritageRecordSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataSource = CachedDataServerFeatureSet::whereName('NI Industrial Heritage Record')->first();

        if ($dataSource) {
            $dataSource->cachedDataServerFeatures()->delete();
        } else {
            $dataSource = CachedDataServerFeatureSet::create([
              'name' => 'NI Industrial Heritage Record',
              'owner' => 'Department of Communities',
              'license_title' => 'UK-OGL',
              'license_url' => 'http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/',
              'uri' => 'https://www.opendatani.gov.uk/dataset/industrial-heritage-record',
              'data_server' => 'opendatani',
              'data_server_set_id' => 'ni-industrial-heritage-record'
            ]);
        }

        $district = District::whereName('Northern Ireland 1')->first();
        $featureType = FeatureSet::whereSlug('industrial-heritage')->first();
        $featureType->districts()->sync([
            $district->id => [
                'data_server_set_id' => $dataSource->data_server_set_id,
                'data_server' => 'opendatani',
                'status' => 0
            ]
        ]);

        $indHeritageJson = json_decode(file_get_contents(base_path() . '/resources/opendata/industrial-heritage-record.geojson'));

        $indHeritageSites = GeoJson::jsonUnserialize($indHeritageJson);
        foreach ($indHeritageSites as $indHeritage) {
            $coordinates = $indHeritage->getGeometry()->getCoordinates();

            $feature = new CachedDataServerFeature;
            $properties = $indHeritage->getProperties();
            $feature->feature_id = $properties['IHR'];
            $feature->location = new Point($coordinates[1], $coordinates[0]);
            $feature->json = json_encode($indHeritage);

            $feature->cached_data_server_feature_set_id = $dataSource->id;

            $feature->save();
        }
    }
}
