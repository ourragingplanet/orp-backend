<?php

use Phaza\LaravelPostgis\Geometries\LineString;
use Phaza\LaravelPostgis\Geometries\Point;
use Phaza\LaravelPostgis\Geometries\Polygon;
use GeoJson\GeoJson;
use Illuminate\Database\Seeder;
use Cocur\Slugify\Slugify;
use App\Models\CachedDataServerFeature;
use App\Models\CachedDataServerFeatureSet;
use App\Models\Features\FeatureSet;
use App\Models\Features\District;

class AreasOfArchaeologicalPotentialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataSource = CachedDataServerFeatureSet::whereName('NI Areas of Archaeological Potential')->first();

        if ($dataSource) {
            $dataSource->cachedDataServerFeatures()->delete();
        } else {
            $dataSource = CachedDataServerFeatureSet::create([
              'name' => 'NI Areas of Archaeological Potential',
              'owner' => 'Department of Communities',
              'license_title' => 'UK-OGL',
              'license_url' => 'http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/',
              'uri' => 'https://www.opendatani.gov.uk/dataset/areas-of-archaeological-potential',
              'data_server' => 'opendatani',
              'data_server_set_id' => 'ni-areas-of-archaeological-potential'
            ]);
        }

        $district = District::whereName('Northern Ireland 1')->first();
        $featureType = FeatureSet::whereSlug('unexplored-cultural-heritage')->first();
        $featureType->districts()->sync([
            $district->id => [
                'data_server_set_id' => $dataSource->data_server_set_id,
                'data_server' => 'opendatani',
                'status' => 1
            ]
        ]);

        $archPotJson = json_decode(file_get_contents(base_path() . '/resources/opendata/areas-of-archaeological-potential.geojson'));

        $archPots = GeoJson::jsonUnserialize($archPotJson);
        foreach ($archPots as $archPot) {
            $coordinates = $archPot->getGeometry()->getCoordinates()[0][0][0];

            $feature = new CachedDataServerFeature;
            $properties = $archPot->getProperties();
            $feature->feature_id = $properties['OBJECTID'];
            $feature->location = new Point($coordinates[1], $coordinates[0]);
            $feature->json = json_encode($archPot);

            $feature->cached_data_server_feature_set_id = $dataSource->id;

            $feature->save();
        }
    }
}
