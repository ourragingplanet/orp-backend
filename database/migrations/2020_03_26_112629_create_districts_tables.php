<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDistrictsTables extends Migration
{
    public function up()
    {
        Schema::table('districts', function (Blueprint $table) {
            
       	    //Necessary columns for Twill
	    $table->dateTime('deleted_at')->nullable();
	    $table->integer('position')->default(0);
	    $table->integer('published')->default(1);
        });

       /* Schema::create('district_revisions', function (Blueprint $table) {
            createDefaultRevisionsTableFields($table, 'district');
        }); */
    }

    public function down()
    {
        Schema::dropIfExists('district_revisions');
	Schema::table('districts', function (Blueprint $table) {
    	$table->dropColumn(['deleted_at', 'position', 'published']);
	});

    }
}
