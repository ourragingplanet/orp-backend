<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MakeOauthClientsUserIdUuid extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Doctrine\DBAL\Types\Type::addType('uuid', 'Ramsey\Uuid\Doctrine\UuidType');
        Schema::table('oauth_clients', function (Blueprint $table) {

            $table->dropColumn('user_id');
        });
        Schema::table('oauth_clients', function (Blueprint $table) {

            $table->uuid('user_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Doctrine\DBAL\Types\Type::addType('uuid', 'Ramsey\Uuid\Doctrine\UuidType');

        if (Schema::hasColumn('user_id', 'oauth_clients')){
            Schema::table('oauth_clients', function (Blueprint $table) {
                $table->dropColumn('user_id');
            });
        }
    }
}
