<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCachedDataServerFeatureSetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cached_data_server_feature_sets', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->string('owner');
            $table->string('license_title');
            $table->string('license_url');
            $table->string('uri');
            $table->string('data_server');
            $table->string('data_server_set_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cached_data_server_feature_sets');
    }
}
