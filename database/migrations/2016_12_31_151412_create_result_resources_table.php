<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('result_resources', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('resource_id')->unsigned();
            $table->foreign('resource_id')->references('id')->on('resources')->onDelete('cascade');

            $table->uuid('simulation_id');
            $table->foreign('simulation_id')->references('id')->on('simulations')->onDelete('cascade');

            $table->decimal('quantity')->nullable();
            $table->integer('duration')->nullable(); // seconds

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('result_resources');
    }
}
