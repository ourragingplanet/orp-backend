<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->uuid('id');

            $table->string('identifier')->unique();
            $table->string('password');

            $table->integer('affiliation_id')->unsigned();
            $table->foreign('affiliation_id')->references('id')->on('affiliations')->onDelete('cascade');

            $table->uuid('teacher_id')->nullable();

            /* Regular users */
            $table->string('forename')->nullable();
            $table->string('surname')->nullable();
            $table->string('email')->nullable();

            $table->rememberToken();
            $table->timestamps();
            $table->primary('id');

            $table->foreign('teacher_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
