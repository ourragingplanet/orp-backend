<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSimulationCountColumnsToAffiliations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('affiliations', function (Blueprint $table) {
            $table->integer('simulations_status')->default(0);
            $table->integer('simulations_remaining')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('affiliations', function (Blueprint $table) {
            $table->dropColumn('simulations_status');
        });

        Schema::table('affiliations', function (Blueprint $table) {
            $table->dropColumn('simulations_remaining');
        });
    }
}
