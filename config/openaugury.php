<?php

return [
    'anonymous-access' => true,
    'brand' => [
        'slug' => 'openaugury',
        'name' => 'OpenAugury',
        'logo' => [
            'long' => env('OPENAUGURY_LOGO_LONG_URL', '/images/logo-text.png'),
            'icon' => env('OPENAUGURY_LOGO_ICON_URL', '/images/logo.png')
        ],
        'splash' => [
            'url-default' => env('OPENAUGURY_SPLASH_URL', '/images/splash.jpg'),
            'url-small' => env('OPENAUGURY_SPLASH_SMALL_URL', '/images/splash-small.jpg'),
            'attribution-text' => env('OPENAUGURY_SPLASH_ATTRIBUTION_TEXT', 'Image attribution'),
            'attribution-url' => env('OPENAUGURY_SPLASH_ATTRIBUTION_URL', 'http://example.com')
        ]
    ],
    'simulation' => [
        'server' => env('SIMULATION_SERVER', '172.17.0.1'),
        'port' => env('SIMULATION_SERVER_PORT', '8080'),
        'maximum_decode_memory' => env('SIMULATION_MAXIMUM_DECODE_MEMORY', '1000M')
    ],
    'legal' => [
        'terms-and-conditions-link' => 'https://s3.eu-west-2.amazonaws.com/ourragingplanet-media/subscription-terms-conditions.pdf',
        'privacy-policy-link' => 'https://s3.eu-west-2.amazonaws.com/ourragingplanet-media/privacy-and-cookies-policy.pdf'
    ],
    'frontend' => [
        'proxy' => env('FRONTEND_PROXY', 'http://172.24.0.1:8180/'),
        'prefix' => 'static/',
        'allow-http' => env('ALLOW_HTTP_URL', 1),
    ]
];
