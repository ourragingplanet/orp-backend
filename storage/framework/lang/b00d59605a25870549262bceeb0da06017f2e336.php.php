<table class="table table-responsive" id="caseFeatureChunks-table">
    <thead>
        <th><?php echo e(_i('Chunk')); ?></th>
        <th><?php echo e(_i('Case Context Id')); ?></th>
        <th><?php echo e(_i('Feature Set Id')); ?></th>
        <th colspan="3"><?php echo e(_i('Action')); ?></th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $caseFeatureChunks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $caseFeatureChunk): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $caseFeatureChunk->chunk; ?></td>
            <td><?php echo $caseFeatureChunk->case_context_id; ?></td>
            <td><?php echo $caseFeatureChunk->feature_set_id; ?></td>
            <td>
                <?php echo Form::open(['route' => ['caseFeatureChunks.destroy', $caseFeatureChunk->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('caseFeatureChunks.show', [$caseFeatureChunk->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('caseFeatureChunks.edit', [$caseFeatureChunk->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>