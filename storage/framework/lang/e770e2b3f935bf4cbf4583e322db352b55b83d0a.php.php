<!-- Text Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('text', 'Text:'); ?>

    <?php echo Form::text('text', null, ['class' => 'form-control']); ?>

</div>

<!-- Subtext Field -->
<div class="form-group col-sm-12 col-lg-12">
    <?php echo Form::label('subtext', 'Subtext:'); ?>

    <?php echo Form::textarea('subtext', null, ['class' => 'form-control']); ?>

</div>

<!-- Informational Type Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('informational_type_id', 'Informational Type Id:'); ?>

    <?php echo Form::number('informational_type_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Case Context Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('case_context_id', 'Case Context Id:'); ?>

    <?php echo Form::text('case_context_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Creator Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('creator_id', 'Creator Id:'); ?>

    <?php echo Form::text('creator_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo route('informationals.index'); ?>" class="btn btn-default">Cancel</a>
</div>
