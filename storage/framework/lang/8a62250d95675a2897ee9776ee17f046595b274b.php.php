<table class="table table-responsive" id="challengeTemplates-table">
    <thead>
        <th><?php echo e(_i('Slug')); ?></th>
        <th><?php echo e(_i('Text')); ?></th>
        <th><?php echo e(_i('Subtext')); ?></th>
        <th><?php echo e(_i('Options')); ?></th>
        <th><?php echo e(_i('Correct')); ?></th>
        <th><?php echo e(_i('Mark')); ?></th>
        <th><?php echo e(_i('Global')); ?></th>
        <th><?php echo e(_i('Analytic Id')); ?></th>
        <th colspan="3"><?php echo e(_i('Action')); ?></th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $challengeTemplates; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $challengeTemplate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $challengeTemplate->slug; ?></td>
            <td><?php echo $challengeTemplate->text; ?></td>
            <td><?php echo $challengeTemplate->subtext; ?></td>
            <td><?php echo $challengeTemplate->options; ?></td>
            <td><?php echo $challengeTemplate->correct; ?></td>
            <td><?php echo $challengeTemplate->mark; ?></td>
            <td><?php echo $challengeTemplate->global; ?></td>
            <td><?php echo $challengeTemplate->analytic_id; ?></td>
            <td>
                <?php echo Form::open(['route' => ['challengeTemplates.destroy', $challengeTemplate->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('challengeTemplates.show', [$challengeTemplate->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('challengeTemplates.edit', [$challengeTemplate->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>