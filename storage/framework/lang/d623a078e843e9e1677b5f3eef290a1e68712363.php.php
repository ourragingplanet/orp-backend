<table class="table table-responsive" id="resultResources-table">
    <thead>
        <th>Resource Id</th>
        <th>Simulation Id</th>
        <th>Quantity</th>
        <th>Duration</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $resultResources; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $resultResource): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $resultResource->resource_id; ?></td>
            <td><?php echo $resultResource->simulation_id; ?></td>
            <td><?php echo $resultResource->quantity; ?></td>
            <td><?php echo $resultResource->duration; ?></td>
            <td>
                <?php echo Form::open(['route' => ['resultResources.destroy', $resultResource->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('resultResources.show', [$resultResource->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('resultResources.edit', [$resultResource->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>