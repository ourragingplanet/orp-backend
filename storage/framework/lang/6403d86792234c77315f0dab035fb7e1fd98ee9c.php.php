<?php $__env->startSection('bodyClass', 'dashboard'); ?>
<?php $__env->startSection('body'); ?>
    <?php
        $urls = \Config::get('panel.panelControllers');
        $linkItems  = \Serverfireteam\Panel\libs\dashboard::getItems();
    ?>

    <div class="loading">
        <h1> LOADING </h1>
        <div class="spinner">
          <div class="rect1"></div>
          <div class="rect2"></div>
          <div class="rect3"></div>
          <div class="rect4"></div>
          <div class="rect5"></div>
        </div>
    </div>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top " role="navigation" style="margin-bottom: 0">

            <!-- /.navbar-header -->
             <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed btn-resp-sidebar" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>

              </div>


            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar " role="navigation">
                <div class="sidebar-nav navbar-collapse collapse " id="bs-example-navbar-collapse-1">
                    <div class="grav center"><img src="/images/logo.png"></div>
                      <div class="user-info"><?php echo e(Auth::user()->first_name.' '.Auth::user()->last_name); ?></div>
                      <a class="visit-site" href="<?php echo e($app['url']->to('/')); ?>"><?php echo e(\Lang::get('panel::fields.visiteSite')); ?>  </a>
                      <ul class="nav" id="side-menu">
                          <li class="<?php echo e((Request::url() === url('panel')) ? 'active' : ''); ?>">
                              <a  href="<?php echo e(url('panel')); ?>" ><i class="fa fa-dashboard fa-fw"></i> <?php echo e(\Lang::get('panel::fields.dashboard')); ?></a>
                          </li>

                          <?php $__currentLoopData = $linkItems; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $linkItem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <?php
                                  $isActive = Request::segment(2) == $linkItem['modelName'];
                              ?>
                              <li class="s-link <?php echo e($isActive ? 'active' : ''); ?>">
                                  <a  href="<?php echo e(url($linkItem['showListUrl'])); ?>" class="<?php echo e($isActive ? 'active' : ''); ?>">
                                      <i class="fa fa-edit fa-fw"></i>
                                      <?php echo e($linkItem['title']); ?>

                                  </a>
                                  <span class="badge <?php echo e(App::getLocale() == 'fa' ? 'pull-left' : 'pull-right'); ?>"><?php echo $linkItem['count']; ?></span>
                                  <div class="items-bar">
                                      <a href="<?php echo e(url($linkItem['addUrl'])); ?>" class="ic-plus" title="Add" ></a>
                                      <a title="List" class="ic-lines" href="<?php echo e(url($linkItem['showListUrl'])); ?>" ></a>
                                  </div>
                              </li>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          <li>
                              <a  href="<?php echo e(url('panel/SimulationTier:Simulation/all')); ?>?shared=1" ><i class="fa fa-help fa-fw"></i> Shared Simulations</a>
                          </li>
                          <li>
                              <a  href="<?php echo e(url('help/lessons')); ?>" ><i class="fa fa-help fa-fw"></i> Lessons</a>
                          </li>
                          <li class="<?php echo e((Request::url() === url('help')) ? 'active' : ''); ?>">
                              <a  href="<?php echo e(url('help')); ?>" ><i class="fa fa-help fa-fw"></i> <?php echo e(\Lang::get('panel::fields.help')); ?></a>
                          </li>

                      </ul>

                        </li>
                    </ul>
                </div>


            </div>
            <!-- /.navbar-static-side -->
        </nav>
        <div class="powered-by"><a href="http://laravelpanel.com"><?php echo e(\Lang::get('panel::fields.thankYouNote')); ?></a></div>
        <div id="page-wrapper">


            <!-- Menu Bar -->
            <div class="row">
                <div class="col-xs-12 text-a top-icon-bar">
                    <div class="btn-group" role="group" aria-label="...">
                        <div class="btn-group" role="group">
                            <a  type="button" class="btn btn-default dropdown-toggle main-link" data-toggle="dropdown" aria-expanded="false">
                                <?php echo e(Lang::get('panel::fields.settings')); ?>

                                <span class="caret"></span>
                            </a>
                          <ul class="dropdown-menu" role="menu">
                            <li><a href="<?php echo e(url('panel/edit')); ?>"><span class="icon  ic-users "></span><?php echo e(Lang::get('panel::fields.ProfileEdit')); ?></a></li>
                            <li><a href="<?php echo e(url('panel/changePassword')); ?>"><span class="icon ic-cog"></span><?php echo e(Lang::get('panel::fields.ChangePassword')); ?></a></li>
                          </ul>
                        </div>
                        <a href="<?php echo e(url('panel/logout')); ?>" type="button" class="btn btn-default main-link"><?php echo e(Lang::get('panel::fields.logout')); ?><span class="icon  ic-switch"></span></a>
                      </div>
                </div>
            </div>

            <?php echo $__env->yieldContent('page-wrapper'); ?>

        </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('panelViews::master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>