<table class="table table-responsive" id="informationalTypes-table">
    <thead>
        <th>Slug</th>
        <th>Name</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $informationalTypes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $informationalType): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $informationalType->slug; ?></td>
            <td><?php echo $informationalType->name; ?></td>
            <td>
                <?php echo Form::open(['route' => ['informationalTypes.destroy', $informationalType->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('informationalTypes.show', [$informationalType->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('informationalTypes.edit', [$informationalType->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>