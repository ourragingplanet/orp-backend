<!-- Id Field -->
<div class="form-group">
    <?php echo Form::label('id', 'Id:'); ?>

    <p><?php echo $cachedDataServerFeatureSet->id; ?></p>
</div>

<!-- Name Field -->
<div class="form-group">
    <?php echo Form::label('name', 'Name:'); ?>

    <p><?php echo $cachedDataServerFeatureSet->name; ?></p>
</div>

<!-- Owner Field -->
<div class="form-group">
    <?php echo Form::label('owner', 'Owner:'); ?>

    <p><?php echo $cachedDataServerFeatureSet->owner; ?></p>
</div>

<!-- License Title Field -->
<div class="form-group">
    <?php echo Form::label('license_title', 'License Title:'); ?>

    <p><?php echo $cachedDataServerFeatureSet->license_title; ?></p>
</div>

<!-- License Url Field -->
<div class="form-group">
    <?php echo Form::label('license_url', 'License Url:'); ?>

    <p><?php echo $cachedDataServerFeatureSet->license_url; ?></p>
</div>

<!-- Uri Field -->
<div class="form-group">
    <?php echo Form::label('uri', 'Uri:'); ?>

    <p><?php echo $cachedDataServerFeatureSet->uri; ?></p>
</div>

<!-- Data Server Field -->
<div class="form-group">
    <?php echo Form::label('data_server', 'Data Server:'); ?>

    <p><?php echo $cachedDataServerFeatureSet->data_server; ?></p>
</div>

<!-- Data Server Set Id Field -->
<div class="form-group">
    <?php echo Form::label('data_server_set_id', 'Data Server Set Id:'); ?>

    <p><?php echo $cachedDataServerFeatureSet->data_server_set_id; ?></p>
</div>

<!-- Created At Field -->
<div class="form-group">
    <?php echo Form::label('created_at', 'Created At:'); ?>

    <p><?php echo $cachedDataServerFeatureSet->created_at; ?></p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    <?php echo Form::label('updated_at', 'Updated At:'); ?>

    <p><?php echo $cachedDataServerFeatureSet->updated_at; ?></p>
</div>

