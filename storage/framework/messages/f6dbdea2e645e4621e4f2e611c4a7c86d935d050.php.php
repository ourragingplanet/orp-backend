<table class="table table-responsive" id="worksheetAnswers-table">
    <thead>
        <th>First</th>
        <th>Final</th>
        <th>Mark</th>
        <th>Worksheet Id</th>
        <th>Challenge Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $worksheetAnswers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $worksheetAnswer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $worksheetAnswer->first; ?></td>
            <td><?php echo $worksheetAnswer->final; ?></td>
            <td><?php echo $worksheetAnswer->mark; ?></td>
            <td><?php echo $worksheetAnswer->worksheet_id; ?></td>
            <td><?php echo $worksheetAnswer->challenge_id; ?></td>
            <td>
                <?php echo Form::open(['route' => ['worksheetAnswers.destroy', $worksheetAnswer->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('worksheetAnswers.show', [$worksheetAnswer->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('worksheetAnswers.edit', [$worksheetAnswer->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>