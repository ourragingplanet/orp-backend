<!-- Id Field -->
<div class="form-group">
    <?php echo Form::label('id', 'Id:'); ?>

    <p><?php echo $informational->id; ?></p>
</div>

<!-- Text Field -->
<div class="form-group">
    <?php echo Form::label('text', 'Text:'); ?>

    <p><?php echo $informational->text; ?></p>
</div>

<!-- Subtext Field -->
<div class="form-group">
    <?php echo Form::label('subtext', 'Subtext:'); ?>

    <p><?php echo $informational->subtext; ?></p>
</div>

<!-- Informational Type Id Field -->
<div class="form-group">
    <?php echo Form::label('informational_type_id', 'Informational Type Id:'); ?>

    <p><?php echo $informational->informational_type_id; ?></p>
</div>

<!-- Case Context Id Field -->
<div class="form-group">
    <?php echo Form::label('case_context_id', 'Case Context Id:'); ?>

    <p><?php echo $informational->case_context_id; ?></p>
</div>

<!-- Creator Id Field -->
<div class="form-group">
    <?php echo Form::label('creator_id', 'Creator Id:'); ?>

    <p><?php echo $informational->creator_id; ?></p>
</div>

<!-- Created At Field -->
<div class="form-group">
    <?php echo Form::label('created_at', 'Created At:'); ?>

    <p><?php echo $informational->created_at; ?></p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    <?php echo Form::label('updated_at', 'Updated At:'); ?>

    <p><?php echo $informational->updated_at; ?></p>
</div>

