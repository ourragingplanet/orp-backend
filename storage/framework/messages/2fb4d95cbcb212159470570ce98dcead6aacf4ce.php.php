<!-- Id Field -->
<div class="form-group">
    <?php echo Form::label('id', 'Id:'); ?>

    <p><?php echo $phenomenon->id; ?></p>
</div>

<!-- Slug Field -->
<div class="form-group">
    <?php echo Form::label('slug', 'Slug:'); ?>

    <p><?php echo $phenomenon->slug; ?></p>
</div>

<!-- Name Field -->
<div class="form-group">
    <?php echo Form::label('name', 'Name:'); ?>

    <p><?php echo $phenomenon->name; ?></p>
</div>

<!-- Symbol Field -->
<div class="form-group">
    <?php echo Form::label('symbol', 'Symbol:'); ?>

    <p><?php echo $phenomenon->symbol; ?></p>
</div>

<!-- Color Field -->
<div class="form-group">
    <?php echo Form::label('color', 'Color:'); ?>

    <p><?php echo $phenomenon->color; ?></p>
</div>

<!-- Created At Field -->
<div class="form-group">
    <?php echo Form::label('created_at', 'Created At:'); ?>

    <p><?php echo $phenomenon->created_at; ?></p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    <?php echo Form::label('updated_at', 'Updated At:'); ?>

    <p><?php echo $phenomenon->updated_at; ?></p>
</div>

