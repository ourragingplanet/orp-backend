<?php $__env->startSection('contentFields'); ?>
    @formField('input', [
        'name' => 'description',
        'label' => 'Description',
        'maxlength' => 100
    ])
<?php $__env->stopSection(); ?>

<?php echo $__env->make('twill::layouts.form', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>