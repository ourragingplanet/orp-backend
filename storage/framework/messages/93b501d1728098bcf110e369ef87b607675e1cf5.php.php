<?php $__env->startSection('content'); ?>
    <section class="content-header">
        <h1>
            Cached Data Server Feature Set
        </h1>
   </section>
   <div class="content">
       <?php echo $__env->make('adminlte-templates::common.errors', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   <?php echo Form::model($cachedDataServerFeatureSet, ['route' => ['cachedDataServerFeatureSets.update', $cachedDataServerFeatureSet->id], 'method' => 'patch']); ?>


                        <?php echo $__env->make('cached_data_server_feature_sets.fields', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                   <?php echo Form::close(); ?>

               </div>
           </div>
       </div>
   </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>