<table class="table table-responsive" id="phenomenons-table">
    <thead>
        <th>Slug</th>
        <th>Name</th>
        <th>Symbol</th>
        <th>Color</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $phenomenons; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $phenomenon): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $phenomenon->slug; ?></td>
            <td><?php echo $phenomenon->name; ?></td>
            <td><?php echo $phenomenon->symbol; ?></td>
            <td><?php echo $phenomenon->color; ?></td>
            <td>
                <?php echo Form::open(['route' => ['phenomenons.destroy', $phenomenon->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('phenomenons.show', [$phenomenon->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('phenomenons.edit', [$phenomenon->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>