<?php $__env->startPush('extra_css'); ?>
<link rel="stylesheet" href="//unpkg.com/leaflet/dist/leaflet.css" />
<?php $__env->stopPush(); ?>

<?php $__env->startPush('extra_js_head'); ?>
<script src="//unpkg.com/leaflet/dist/leaflet.js"></script>
<script src="//unpkg.com/vue2-leaflet"></script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('twill::layouts.form', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>