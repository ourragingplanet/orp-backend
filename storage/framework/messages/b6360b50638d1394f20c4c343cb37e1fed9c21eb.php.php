<table class="table table-responsive" id="challenges-table">
    <thead>
        <th>Text</th>
        <th>Subtext</th>
        <th>Options</th>
        <th>Correct</th>
        <th>Mark</th>
        <th>Challenge Template Id</th>
        <th>Case Context Id</th>
        <th>Result Analytic Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $challenges; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $challenge): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $challenge->text; ?></td>
            <td><?php echo $challenge->subtext; ?></td>
            <td><?php echo $challenge->options; ?></td>
            <td><?php echo $challenge->correct; ?></td>
            <td><?php echo $challenge->mark; ?></td>
            <td><?php echo $challenge->challenge_template_id; ?></td>
            <td><?php echo $challenge->case_context_id; ?></td>
            <td><?php echo $challenge->result_analytic_id; ?></td>
            <td>
                <?php echo Form::open(['route' => ['challenges.destroy', $challenge->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('challenges.show', [$challenge->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('challenges.edit', [$challenge->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>