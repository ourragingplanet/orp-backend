<!-- Slug Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('slug', 'Slug:'); ?>

    <?php echo Form::text('slug', null, ['class' => 'form-control']); ?>

</div>

<!-- Text Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('text', 'Text:'); ?>

    <?php echo Form::text('text', null, ['class' => 'form-control']); ?>

</div>

<!-- Subtext Field -->
<div class="form-group col-sm-12 col-lg-12">
    <?php echo Form::label('subtext', 'Subtext:'); ?>

    <?php echo Form::textarea('subtext', null, ['class' => 'form-control']); ?>

</div>

<!-- Options Field -->
<div class="form-group col-sm-12 col-lg-12">
    <?php echo Form::label('options', 'Options:'); ?>

    <?php echo Form::textarea('options', null, ['class' => 'form-control']); ?>

</div>

<!-- Correct Field -->
<div class="form-group col-sm-12 col-lg-12">
    <?php echo Form::label('correct', 'Correct:'); ?>

    <?php echo Form::textarea('correct', null, ['class' => 'form-control']); ?>

</div>

<!-- Mark Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('mark', 'Mark:'); ?>

    <?php echo Form::number('mark', null, ['class' => 'form-control']); ?>

</div>

<!-- Analytic Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('analytic_id', 'Analytic Id:'); ?>

    <?php echo Form::number('analytic_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo route('challengeTemplates.index'); ?>" class="btn btn-default">Cancel</a>
</div>
