<!-- Identifier Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('identifier', 'Identifier:'); ?>

    <?php echo Form::text('identifier', null, ['class' => 'form-control']); ?>

</div>

<!-- Password Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('password', 'Password:'); ?>

    <?php echo Form::password('password', ['class' => 'form-control']); ?>

</div>

<!-- Affiliation Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('affiliation_id', 'Affiliation Id:'); ?>

    <?php echo Form::number('affiliation_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Teacher Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('teacher_id', 'Teacher Id:'); ?>

    <?php echo Form::text('teacher_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Forename Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('forename', 'Forename:'); ?>

    <?php echo Form::text('forename', null, ['class' => 'form-control']); ?>

</div>

<!-- Surname Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('surname', 'Surname:'); ?>

    <?php echo Form::text('surname', null, ['class' => 'form-control']); ?>

</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('email', 'Email:'); ?>

    <?php echo Form::email('email', null, ['class' => 'form-control']); ?>

</div>

<!-- Remember Token Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('remember_token', 'Remember Token:'); ?>

    <?php echo Form::text('remember_token', null, ['class' => 'form-control']); ?>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo route('users.index'); ?>" class="btn btn-default">Cancel</a>
</div>
