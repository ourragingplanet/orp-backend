<?php $__env->startSection('page-wrapper'); ?>

            <div class="row">

                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo e(\Lang::get('panel::fields.help')); ?></h1>
                    <div class="icon-bg ic-layers"></div>
                </div>

                <?php if(Request::path() != 'help'): ?>
                <h4><a href='/help'>Return to Help Menu</a></h4>
                <?php endif; ?>

            </div>
            <!-- /.row -->
            <div class="row box-holder" class="help-content">
                        <?php echo $__env->yieldContent('content'); ?>
            </div>

<script>
$(function(){
    var color = ['primary','green','orange','red','purple','green2','blue2','yellow'];
    var pointer = 0;
    $('.panel').each(function(){
        if(pointer > color.length)
            pointer = 0;
        $(this).addClass('panel-'+color[pointer]);
        $(this).find('.pull-right .add').addClass('panel-'+color[pointer]);
        pointer++;
    })
})
</script>
<style>
.help-figure {
    text-align: center;
    width: 100%;
}
.help-figure img {
    display: block;
    margin-top: 30px;
    margin-left: auto;
    margin-right: auto;
    max-width: 50%;
    box-shadow: 0 0 5px 5px #aaa;
}
.help-figure p {
    display: inline-block;
    margin: 20px;
    padding: 20px;
    padding-top: 0px;
    padding-bottom: 10px;
    border-top: thin black solid;
    background-color: #ddd;
}
h2, h3, h4 {
    color: #555;
    background-color: #eee;
    border-bottom: thin black solid;
}
</style>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('panelViews::mainTemplate', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>