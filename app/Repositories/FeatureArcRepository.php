<?php

namespace App\Repositories;

use App\Models\SimulationTier\FeatureArc;
use InfyOm\Generator\Common\BaseRepository;

class FeatureArcRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'feature_chunk_id',
        'simulation_id',
        'arc'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return FeatureArc::class;
    }
}
