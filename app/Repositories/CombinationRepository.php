<?php

namespace App\Repositories;

use App\Models\AbstractTier\Combination;
use App\Models\Users\Affiliation;
use A17\Twill\Repositories\ModuleRepository;

class CombinationRepository extends ModuleRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'status',
        'numerical_model_id',
        'owner_id'
    ];

    /**
     * Configure the Model
     **/
    public function __construct(Combination $model)
    {
        $this->model = $model;
    }

    public function getActiveForAffiliation(Affiliation $affiliation)
    {
        return $this->model
                    ->forAffiliation($affiliation)
                    ->active()
                    ->get();
    }
}
