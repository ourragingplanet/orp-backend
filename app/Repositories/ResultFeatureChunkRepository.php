<?php

namespace App\Repositories;

use App\Models\SimulationTier\ResultFeatureChunk;
use InfyOm\Generator\Common\BaseRepository;

class ResultFeatureChunkRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'chunk',
        'simulation_id',
        'feature_set_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ResultFeatureChunk::class;
    }
}
