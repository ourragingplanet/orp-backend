<?php

namespace App\Transformers;

use App\Models\Users\User;
use App\Models\Users\Affiliation;
use League\Fractal;

class UserTransformer extends Fractal\TransformerAbstract
{
    protected $availableIncludes = [
            'affiliation'
    ];

    protected $options = [
        'withDefaultLocation' => false
    ];

    public function setOption($option, $state) {
        $this->options[$option] = $state;
    }

    public function transform(User $user)
    {
        $userArray = $user->toArray();

        $data = [
            'id' => $userArray['id'],
            'forename' => $userArray['forename'],
            'surname' => $userArray['surname'],
            'email' => $userArray['email']
        ];

        if ($this->options['withDefaultLocation']) {
            $data['defaultLocation'] = $user->default_location;
        }

        return $data;
    }

    public function includeAffiliation(User $user)
    {
        $affiliation = $user->affiliation;

        if (! $affiliation) {
            return null;
        }

        return $this->item($affiliation, new AffiliationTransformer, 'affiliations');
    }
}
