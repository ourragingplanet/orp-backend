<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class DistrictController extends ModuleController
{
    protected $moduleName = 'districts';

    protected $titleColumnKey = 'name';

    protected $indexColumns = [
        'name' => [ // field column
            'title' => 'Name',
            'field' => 'name',
        ],
    ];
}
