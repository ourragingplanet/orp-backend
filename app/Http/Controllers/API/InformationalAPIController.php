<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Interaction\Informational;
use App\Repositories\InformationalRepository;


class InformationalAPIController extends Controller
{
    /**
     * @var InformationalRepository
     */


    public function __construct(InformationalRepository $InformationalRepo)
    {
        $this->InformationalRepository = $InformationalRepo;

    }//end __construct()


    private $InformationalRepository;


    public function getNewsFile($InformationalID) {
        $Informational = $this->InformationalRepository->getById($InformationalID);
        $FileID = $Informational->files->pluck('id');
        $fileObject = $Informational->files()->find($FileID);
        return \Storage::disk('twill-s3')->temporaryUrl($fileObject->uuid, now()->addMinutes(5));
   
    }
}
