<?php

namespace App\Http\Controllers\API;
use Carbon\Carbon;
use Swis\JsonApi\Client\Parsers\DocumentParser;
use App\Http\Requests\API\CreateSimulationAPIRequest;
use App\Http\Requests\API\UpdateSimulationAPIRequest;
use App\Models\SimulationTier\Simulation;
use App\Repositories\SimulationRepository;
use App\Repositories\CombinationRepository;
use App\Repositories\CaseContextRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Transformers\SimulationTransformer;
use App\Transformers\SimulationStatisticsTransformer;
use App\Transformers\ResultFeatureChunkTransformer;
use App\Transformers\InformationalTransformer;
use App\Transformers\ChallengeTransformer;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Symfony\Component\HttpFoundation\Response;
use DB;
use Auth;
use Log;
use Spatie\Fractal\Fractal;
use App\Jobs\SimulationRun;

/**
 * Class SimulationController
 * @package App\Http\Controllers\API
 */

class SimulationAPIController extends AppBaseController
{
    /** @var  SimulationRepository */
    private $simulationRepository;

    /** @var  CaseContextRepository */
    private $caseContextRepository;

    /** @var  CombinationRepository */
    private $combinationRepository;

    public function __construct(SimulationRepository $simulationRepo, CaseContextRepository $caseContextRepo, CombinationRepository $combinationRepo)
    {
        $this->simulationRepository = $simulationRepo;
        $this->caseContextRepository = $caseContextRepo;
        $this->combinationRepository = $combinationRepo;
        $this->auth = app()->make('auth');
        $this->authorizeResource(Simulation::class, 'simulation');
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/simulations",
     *      summary="Get a listing of the Simulations.",
     *      tags={"Simulation"},
     *      description="Get all Simulations",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Simulation")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function index(Request $request)
    {
        // $this->simulationRepository->pushCriteria(new RequestCriteria($request));
        // $this->simulationRepository->pushCriteria(new LimitOffsetCriteria($request));
        $simulations = $this->simulationRepository->all();

        return fractal($simulations, new SimulationTransformer)
                ->withResourceName('simulations')->respond();
    }

    /**
     * @param CreateSimulationAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/simulations",
     *      summary="Store a newly created Simulation in storage",
     *      tags={"Simulation"},
     *      description="Store Simulation",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Simulation that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Simulation")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Simulation"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function store(CreateSimulationAPIRequest $request, DocumentParser $parser)
    {
        $input = $request->all();

        $input['data']['id'] = '[none]';
        foreach (['relationships', 'attributes'] as $property) {
            if (array_key_exists($property, $input['data']) && $input['data'][$property] == []) {
                $input['data'][$property] = new \stdClass();
            }
        }
        foreach (['links', 'included'] as $property) {
            if (array_key_exists($property, $input) && $input[$property] == []) {
                $input[$property] = new \stdClass();
            }
        }
        if (array_key_exists('relationships', $input['data'])) {
            foreach ($input['data']['relationships'] as $rel => &$target) {
                if (array_key_exists('links', $target)) {
                    unset($target['links']);
                }
            }
            $input['data']['relationships'] = json_decode(
                json_encode(
                    $input['data']['relationships'],
                    JSON_FORCE_OBJECT
                )
            );
        }
        \Log::info($input);
        $document = $parser->parse(json_encode($input));

        $data = $document->getData();
        $combination = $data->getRelations()['combination']->getIncluded();
        $caseContext = $data->getRelations()['caseContext']->getIncluded();
        $input = $data->toArray();
        $input['case_context_id'] = $this->caseContextRepository->find($caseContext->id)->id;

        $combination = $this->combinationRepository->find($combination->id);

        if (! $combination->canIUse()) {
            return [
                'success' => False,
                'msg' => _("This combination is not currently available for use")
            ];
        }

        $input['combination_id'] = $combination->id;
        $input['owner_id'] = $this->auth->user()->id;
        $input['begins'] = Carbon::parse($input['begins']);
        $input['ends'] = Carbon::parse($input['ends']);

        if (array_key_exists('settings', $input)) {
            $input['settings'] = json_encode($input['settings']);
        } else {
            $input['settings'] = '{}';
        }

        if (array_key_exists('result', $input)) {
            $input['result'] = json_encode($input['result']);
        } else {
            $input['result'] = '{}';
        }

        if (array_key_exists('definition', $input)) {
            $input['definition'] = json_encode($input['definition']);
        } else {
            $input['definition'] = '{}';
        }

        $input['status'] = Simulation::STATUS_INACTIVE;

        $simulation = $this->simulationRepository->create($input);

        return fractal($simulation, new SimulationTransformer)
                ->withResourceName('simulations')->respond();
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/simulations/{id}",
     *      summary="Display the specified Simulation",
     *      tags={"Simulation"},
     *      description="Get Simulation",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Simulation",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Simulation"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function show(Simulation $simulation)
    {
        return fractal($simulation, new SimulationTransformer)
                ->withResourceName('simulations')->respond();
    }

    /**
     * @param int $id
     * @return Response
     *
     */
    public function statisticsIndex(Request $request, $id)
    {
        $simulation = $this->simulationRepository->getById($id);

        if (empty($simulation)) {
            return $this->sendError('Simulation not found');
        }

        return fractal($simulation, new SimulationStatisticsTransformer)
                ->withResourceName('simulationStatistics')->respond();
    }

    /**
     * @param int $id
     * @param UpdateSimulationAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/simulations/{id}",
     *      summary="Update the specified Simulation in storage",
     *      tags={"Simulation"},
     *      description="Update Simulation",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Simulation",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Simulation that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Simulation")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Simulation"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function update(Simulation $simulation, UpdateSimulationAPIRequest $request)
    {
        $input = $request->all();

        if (array_key_exists('settings', $input)) {
            $input['settings'] = json_encode($input['settings']);
        }

        if (array_key_exists('result', $input)) {
            $input['result'] = json_encode($input['result']);
        }

        $simulation = $this->simulationRepository->update($input, $simulation->id);

        return fractal($simulation, new SimulationTransformer)
                ->withResourceName('simulations')->respond();
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/simulations/{id}",
     *      summary="Remove the specified Simulation from storage",
     *      tags={"Simulation"},
     *      description="Delete Simulation",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Simulation",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function destroy(Simulation $simulation)
    {
        $user = $this->auth->user();
        if (
            $simulation->owner_id == $user->id
            || $user->can('delete any Simulations')
        ) {
            $this->simulationRepository->delete($simulation->id);

            return response(['success' => True], 200);
        } else {
            return response(['message' => 'No permission to delete this simulation'], 401);
        }
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/simulations/{id}/feature-arcs/{featureArcId}",
     *      summary="Display the specified Simulation",
     *      tags={"Simulation"},
     *      description="Get Simulation Feature arc",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Simulation",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="featureArcId",
     *          description="id of Feature Arc",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="json"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function featureArcShow($id, $featureArcId)
    {
        /** @var Simulation $simulation */
        $simulation = $this->simulationRepository->with(['featureArcs'])->find($id);

        if (empty($simulation)) {
            return $this->sendError('Simulation not found');
        }

        $featureArc = $simulation
            ->featureArcs()
            ->where('feature_arcs.id', '=', $featureArcId)
            ->first();

        if (empty($featureArc)) {
            return $this->sendError('Feature arc not found');
        }

        return response($featureArc->arc, 200)
            ->header('Content-Type', 'application/json');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/simulations/{id}/features/{featureChunkId}",
     *      summary="Display the specified ResultFeatureChunk JSON",
     *      tags={"Simulation"},
     *      description="Get Simulation Feature chunk",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Simulation",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="featureChunkId",
     *          description="id of ResultFeatureChunk",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="json"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function featuresShow($id, $featureChunkId)
    {
        $simulation = $this->simulationRepository->getById($id, ['resultFeatureChunks']);

        if (empty($simulation)) {
            return $this->sendError('Simulation not found');
        }

        $resultFeatureChunk = $simulation
            ->resultFeatureChunks()
            ->where('result_feature_chunks.id', '=', $featureChunkId)
            ->first();
        return response($resultFeatureChunk->arc, 200)
            ->header('Content-Type', 'application/json');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/simulations/{id}/features",
     *      summary="Display the calculated features of the specified Simulation",
     *      tags={"Simulation"},
     *      description="Get Simulation Features",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Simulation",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="array",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ResultFeatureChunk"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function featuresIndex(Request $request, $id)
    {
        /** @var Simulation $simulation */
        $simulation = $this->simulationRepository->getById($id, ['resultFeatureChunks']);

        if (empty($simulation)) {
            return $this->sendError('Simulation not found');
        }

        $resultFeatureChunks = $simulation->resultFeatureChunks();

        if ($request->has('time_offset')) {
            // Note that the (int) is _required_ to sanitize
            $integerTimeOffset = (int)$request->get('time_offset');
            $resultFeatureChunks = $resultFeatureChunks->orderBy(DB::raw('ABS(time_offset - ' . $integerTimeOffset . ')'));
        }

        $resultFeatureChunks = $resultFeatureChunks
            ->limit(2)
            ->get()
            ->sortBy('time_offset');

        return fractal($resultFeatureChunks, new ResultFeatureChunkTransformer)
                ->withResourceName('resultFeatureChunks')->respond();
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Post(
     *      path="/simulations/{id}/run",
     *      summary="Run the specified Simulation",
     *      tags={"Simulation"},
     *      description="Run Simulation",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Simulation",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="force",
     *          description="overwrite existing results",
     *          type="boolean",
     *          required=false
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Simulation"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function run(Request $request, $id)
    {
        /** @var Simulation $simulation */
        $simulation = $this->simulationRepository->getById($id, ['resultFeatureChunks']);

        $user = $this->auth->user();

        if (empty($simulation)) {
            return $this->sendError('Simulation not found');
        }

        $force = $request->has('force') && $request->get('force');
        if ($force) {
            $simulation->status = Simulation::STATUS_UNPROCESSED;
            $simulation->save();
        }

        dispatch(new SimulationRun($simulation, $user));

        return fractal($simulation, new SimulationTransformer)
                ->withResourceName('simulations')->respond();
    }

    /** Informationals **/

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/simulations/{id}/informationals",
     *      summary="Get a listing of the Informationals for a Simulation.",
     *      tags={"Simulation", "Informational"},
     *      description="Get all Informationals for a Simulation",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Informational",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Informational")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function informationalIndex(Request $request, $id)
    {
        /** @var Simulation $simulation */
        $simulation = $this->simulationRepository->getById($id);

        if (empty($simulation)) {
            return $this->sendError('Simulation not found');
        }

        $informationals = $simulation->informationals->sortBy('time_offset');

        return fractal($informationals, new InformationalTransformer)
                ->withResourceName('informationals')->respond();
    }

    /** Challenges **/

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/simulations/{id}/challenges",
     *      summary="Get a listing of the Challenges for a Simulation.",
     *      tags={"Simulation", "Challenge"},
     *      description="Get all Challenges for a Simulation",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Challenge",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Challenge")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function challengeIndex(Request $request, $id)
    {
        /** @var Simulation $simulation */
        $simulation = $this->simulationRepository->getById($id);

        if (empty($simulation)) {
            return $this->sendError('Simulation not found');
        }

        $challenges = $simulation->challenges->sortBy('time_offset');

        return fractal($challenges, new ChallengeTransformer)
                ->withResourceName('challenges')->respond();
    }

    public function cloneSimulation(Request $request)
    {
        $simulation = $this->simulationRepository->getById($request->input('id'));
        $cloneSimulation = $simulation->replicate();
        $cloneSimulation->owner_id = $request->input('userId');
        $cloneSimulation->created_at = Carbon::now();
        $cloneSimulation->save();
        return $cloneSimulation;
    }

    
}
