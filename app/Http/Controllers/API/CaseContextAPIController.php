<?php

namespace App\Http\Controllers\API;

use Auth;
use App\Http\Requests\API\CreateCaseContextAPIRequest;
use App\Http\Requests\API\UpdateCaseContextAPIRequest;
use App\Models\CaseTier\CaseContext;
use App\Transformers\CaseContextTransformer;
use App\Transformers\InformationalTransformer;
use App\Models\Interaction\Informational;
use App\Repositories\CaseContextRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Phaza\LaravelPostgis\Geometries\Point;
use Phaza\LaravelPostgis\Geometries\LineString;
use Phaza\LaravelPostgis\Geometries\Polygon;
use Symfony\Component\HttpFoundation\Response;
use Spatie\Fractal\Fractal;
use Swis\JsonApi\Client\Parsers\DocumentParser;

/**
 * Class CaseContextController
 * @package App\Http\Controllers\API
 */

class CaseContextAPIController extends AppBaseController
{
    /** @var  CaseContextRepository */
    private $caseContextRepository;

    public function __construct(CaseContextRepository $caseContextRepo, Fractal $fractal)
    {
        $this->caseContextRepository = $caseContextRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/case-contexts",
     *      summary="Get a listing of the CaseContexts.",
     *      tags={"CaseContext"},
     *      description="Get all CaseContexts",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/CaseContext")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function index(Request $request)
    {
        // $this->caseContextRepository->pushCriteria(new RequestCriteria($request));
        // $this->caseContextRepository->pushCriteria(new LimitOffsetCriteria($request));
        
        /* $user = Auth::user();
        $caseContexts = CaseContext::with('owner')->get()->filter(function ($caseContext) use($user){
            return $user->can('view', $caseContext);
        }); */

        $caseContexts = $this->caseContextRepository->all();

        return fractal($caseContexts, new CaseContextTransformer)
            ->parseExcludes('features')
            ->withResourceName('caseContexts')
            ->respond();
    }

    /**
     * @param CreateCaseContextAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/case-contexts",
     *      summary="Store a newly created CaseContext in storage",
     *      tags={"CaseContext"},
     *      description="Store CaseContext",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CaseContext that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CaseContext")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CaseContext"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function store(CreateCaseContextAPIRequest $request, DocumentParser $parser)
    {
        $input = $request->all();
        $input['data']['id'] = '[none]';
        if (array_key_exists('relationships', $input['data']) && $input['data']['relationships'] == []) {
            $input['data']['relationships'] = new \stdClass();
        }
        $document = $parser->parse(json_encode($input));

        $data = $document->getData();
        $input = $data->toArray();
        $center = [
            $input['center']->lat, $input['center']->lng
        ];
        $input['centerLatLng'] = $center;
        unset($input['center']);

        // TODO: needs improved, with ACL
        $input['name'] = 'My Area #' . $this->caseContextRepository->getCountForAll();

        $user = Auth::user();
        if (!$user->can('pick district')) {
            $input['district_id'] = $user->affiliation->district_id;
            if (!$input['district_id']) {
                abort(422, ['You must have an affiliation with a valid district']);
            }
        }

        $input['begins'] = \Carbon\Carbon::now();
        $input['ends'] = \Carbon\Carbon::now()->addHours(6);

        $width = 0.01;
        $height = 0.01;
        $extent = new Polygon([new LineString([
          new Point($center[1] + $width / 2, $center[0] - $height / 2),
          new Point($center[1] - $width / 2, $center[0] - $height / 2),
          new Point($center[1] - $width / 2, $center[0] + $height / 2),
          new Point($center[1] + $width / 2, $center[0] + $height / 2),
          new Point($center[1] + $width / 2, $center[0] - $height / 2),
        ])]);
        $input['extent'] = $extent;
        $caseContext = $this->caseContextRepository->create($input);

        return fractal($caseContext, new CaseContextTransformer)
            ->withResourceName('caseContexts')
            ->respond();
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/case-contexts/{id}",
     *      summary="Display the specified CaseContext",
     *      tags={"CaseContext"},
     *      description="Get CaseContext",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CaseContext",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CaseContext"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function show($id)
    {
        /** @var CaseContext $caseContext */
        $caseContext = $this->caseContextRepository->find($id);

        if (empty($caseContext)) {
            return $this->sendError('Case Context not found');
        }

        return fractal($caseContext, new CaseContextTransformer)
            ->withResourceName('caseContexts')
            ->includeFeatureSets()
            ->respond();
    }

    /**
     * @param int $id
     * @param UpdateCaseContextAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/case-contexts/{id}",
     *      summary="Update the specified CaseContext in storage",
     *      tags={"CaseContext"},
     *      description="Update CaseContext",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CaseContext",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CaseContext that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CaseContext")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CaseContext"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function update($id, UpdateCaseContextAPIRequest $request)
    {
        $input = $request->all();

        /** @var CaseContext $caseContext */
        $caseContext = $this->caseContextRepository->find($id);

        if (empty($caseContext)) {
            return $this->sendError('Case Context not found');
        }

        $caseContext = $this->caseContextRepository->update($input, $id);

        return fractal($caseContext, new CaseContextTransformer)
            ->withResourceName('caseContexts')
            ->includeFeatureSets()
            ->respond();
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/case-contexts/{id}",
     *      summary="Remove the specified CaseContext from storage",
     *      tags={"CaseContext"},
     *      description="Delete CaseContext",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CaseContext",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function destroy($id)
    {
        /** @var CaseContext $caseContext */
        $caseContext = $this->caseContextRepository->find($id);

        if (empty($caseContext)) {
            return $this->sendError('Case Context not found');
        }

        $this->caseContextRepository->delete($id);

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
