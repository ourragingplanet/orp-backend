<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class TargetZoneRevision extends Revision
{
    protected $table = "target_zone_revisions";
}
