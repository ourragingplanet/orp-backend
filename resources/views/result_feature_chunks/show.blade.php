@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
        {{ _i('Result Feature Chunk') }}
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('result_feature_chunks.show_fields')
                    <a href="{!! route('resultFeatureChunks.index') !!}" class="btn btn-default">{{ _i('Back') }}</a>
                </div>
            </div>
        </div>
    </div>
@endsection
