@extends('help.template')
@section('content')

<h2>Managing Informationals</h2>

<p>Informationals can be added, edited and
removed through your Simulation's Informational list. Open the edit dialog for
your Simulation by clicking the <em>Simulation</em> item in the left-hand
navigation menu, and clicking its name in the Simulation index. There is a link
<em>Add or edit informationals</em> in the Edit Simulation page, this will take
you to the Simulation's Informational list.</p>

<div class='help-figure'>
    <img alt="Listing all of your Informationals" src='/images/screenshots/screenshot-as-informationals.png'/>
    <p>Figure: Listing your <em>Informationals</em></p>
</div>

<p>To add a new Informational,
click the <em>Add</em> button at the top-right. There are three available types
of Informational, which will be formatted slightly differently. These are:
<em>Information</em>, <em>Newsflash</em> and <em>Social Media</em>. Information
is intended for general information or facts that may be useful for students.
Newsflashes are intended to help students to better relate to newsflashes that
may be seen on television or online news reporting services during emergencies.
Social media informationals provide a means to bring individual-scale
perspectives into the student's view of the unfolding disaster.</p>

<p>Informationals are shown in the <em>News Feed</em> popup on the bottom left
of the Map Viewer, when clicked by students. The <em>Title</em> will be shown
in bold text at the top of the popup. The appearance of the <em>Author to
Display</em> depends on the type of informational - for newsflashes you may
wish to use a (possibly invented) media outlet, for social media, a fake
username in the format <em>@somename</em> is recommended. The <em>Seconds after
Simulation start</em> lets you choose where in the Timeline it appears.
<em>Content</em> is the text that will appear in the body of the Informational
- it is recommended that this is kept as succinct as possible.</p>

<div class='help-figure'>
    <img alt="Informational in the Map Viewer" src='/images/screenshots/screenshot-newsfeed.png'/>
    <p>Figure: an <em>Informational</em> being displayed in the <em>Map Viewer</em></p>
</div>

@endsection
