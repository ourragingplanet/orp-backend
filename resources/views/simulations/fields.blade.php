<!-- Case Context Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('case_context_id', 'Case Context Id:') !!}
    {!! Form::text('case_context_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Combination Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('combination_id', 'Combination Id:') !!}
    {!! Form::text('combination_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Settings Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('settings', 'Settings:') !!}
    {!! Form::textarea('settings', null, ['class' => 'form-control']) !!}
</div>

<!-- Definition Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('definition', 'Definition:') !!}
    {!! Form::textarea('definition', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::number('status', null, ['class' => 'form-control']) !!}
</div>

<!-- Center Field -->
<div class="form-group col-sm-6">
    {!! Form::label('center', 'Center:') !!}
    {!! Form::text('center', null, ['class' => 'form-control']) !!}
</div>

<!-- Extent Field -->
<div class="form-group col-sm-6">
    {!! Form::label('extent', 'Extent:') !!}
    {!! Form::text('extent', null, ['class' => 'form-control']) !!}
</div>

<!-- Begins Field -->
<div class="form-group col-sm-6">
    {!! Form::label('begins', 'Begins:') !!}
    {!! Form::date('begins', null, ['class' => 'form-control']) !!}
</div>

<!-- Ends Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ends', 'Ends:') !!}
    {!! Form::date('ends', null, ['class' => 'form-control']) !!}
</div>

<!-- Result Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('result', 'Result:') !!}
    {!! Form::textarea('result', null, ['class' => 'form-control']) !!}
</div>

<!-- Owner Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('owner_id', 'Owner Id:') !!}
    {!! Form::text('owner_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('simulations.index') !!}" class="btn btn-default">Cancel</a>
</div>
