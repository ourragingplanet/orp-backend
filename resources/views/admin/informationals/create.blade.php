@formField('select', [
    'name' => 'simulation_id',
    'label' => 'Simulation',
    'placeholder' => 'Select a Simulation',
    'options' => $simulations
])



@formField('select', [
    'name' => 'informational_type_id',
    'label' => 'Informational Type',
    'placeholder' => 'Select a Informational Type',
    'options' => $informationalTemplates,
    'required' => true,
])

@formField('input', [
    'name' => 'text',
    'label' => 'Title',
    'placeholder' => 'Enter header text'
])


@formField('wysiwyg', [
    'name' => 'subtext',
    'label' => 'Description',
    'placeholder' => 'Enter description',
    'toolbarOptions' => [
      ['header' => [2, 3, 4, 5, 6, false]],
      'bold',
      'italic',
      'underline',
      'strike',
      ["direction" => "rtl"]],
      'required' => true,
])

@formField('date_picker', [
'name' => 'time',
'label' => 'Time',
//'minDate' => $futureDateTime
//'maxDate' => '2017-12-10 12:00'
])


