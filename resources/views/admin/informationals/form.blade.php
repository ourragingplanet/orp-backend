@extends('twill::layouts.form')

@push('extra_css')
<link rel="stylesheet" href="//unpkg.com/leaflet/dist/leaflet.css" />
@endpush

@push('extra_js_head')
<script src="//unpkg.com/leaflet/dist/leaflet.js"></script>
<script src="//unpkg.com/vue2-leaflet"></script>
@endpush


@section('contentFields')
@formField('input', [
    'name' => 'url',
    'label' => 'Website URL',
    'placeholder' => 'http://www.website-project.com',
    'note' => 'Please enter full URL.',
    'maxlength' => 200
])

@formField('files', [
    'name' => 'newsFile',
    'label' => 'File',
    'note' => 'Supported Formats; .pdf, .doc, .txt',
    'max' => 4,
])

@formField('wysiwyg', [
    'name' => 'subtext',
    'label' => 'Description',
    'placeholder' => 'Enter description',
    'toolbarOptions' => [
      ['header' => [2, 3, 4, 5, 6, false]],
      'bold',
      'italic',
      'underline',
      'strike',
      ["direction" => "rtl"]],
      'required' => true,
])
@stop
