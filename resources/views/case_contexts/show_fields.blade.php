<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $caseContext->id !!}</p>
</div>

<!-- District Id Field -->
<div class="form-group">
    {!! Form::label('district_id', 'District Id:') !!}
    <p>{!! $caseContext->district_id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $caseContext->name !!}</p>
</div>

<!-- Center Field -->
<div class="form-group">
    {!! Form::label('center', 'Center:') !!}
    <p>{!! $caseContext->center !!}</p>
</div>

<!-- Extent Field -->
<div class="form-group">
    {!! Form::label('extent', 'Extent:') !!}
    <p>{!! $caseContext->extent !!}</p>
</div>

<!-- Begins Field -->
<div class="form-group">
    {!! Form::label('begins', 'Begins:') !!}
    <p>{!! $caseContext->begins !!}</p>
</div>

<!-- Ends Field -->
<div class="form-group">
    {!! Form::label('ends', 'Ends:') !!}
    <p>{!! $caseContext->ends !!}</p>
</div>

<!-- Owner Id Field -->
<div class="form-group">
    {!! Form::label('owner_id', 'Owner Id:') !!}
    <p>{!! $caseContext->owner_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $caseContext->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $caseContext->updated_at !!}</p>
</div>

