@extends('panelViews::mainTemplate')
@section('page-wrapper')

    @if ($helper_message)
	<div>&nbsp;</div>
	<div class="alert alert-info">
		<h3 class="help-title">{{ trans('rapyd::rapyd.help') }}</h3>
		{{ $helper_message }}
	</div>
    @endif

    <p>
        {!! $form !!}
    </p>
{{--
    @if ($form->model)
	<h3>Informationals</h3>
	<table width=100%>
	    <thead>
		<tr><th width=20%>Type</th><th>Text</th></tr>
	    </thead>
	    <tbody>
	    @foreach ($form->model->informationals as $informational)
		<tr><td>{{ $informational->type }}</td><td><strong>{{ $informational->text }}</strong><br/>{{ $information->subtext }}</tr>
	    @endforeach
	    </tbody>
	</table>
	<button class='btn'>Add Informational</button>
    @endif
--}}
@stop
