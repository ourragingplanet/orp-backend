<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $worksheet->id !!}</p>
</div>

<!-- Progress Field -->
<div class="form-group">
    {!! Form::label('progress', 'Progress:') !!}
    <p>{!! $worksheet->progress !!}</p>
</div>

<!-- Case Context Id Field -->
<div class="form-group">
    {!! Form::label('case_context_id', 'Case Context Id:') !!}
    <p>{!! $worksheet->case_context_id !!}</p>
</div>

<!-- Worker Id Field -->
<div class="form-group">
    {!! Form::label('worker_id', 'Worker Id:') !!}
    <p>{!! $worksheet->worker_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $worksheet->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $worksheet->updated_at !!}</p>
</div>

