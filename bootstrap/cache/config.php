<?php return array (
  'activitylog' => 
  array (
    'enabled' => true,
    'delete_records_older_than_days' => 365,
    'default_log_name' => 'default',
    'default_auth_driver' => NULL,
    'subject_returns_soft_deleted_models' => true,
    'activity_model' => 'Spatie\\Activitylog\\Models\\Activity',
    'table_name' => 'activity_log',
    'database_connection' => NULL,
  ),
  'app' => 
  array (
    'name' => 'OpenAugury',
    'env' => 'local',
    'debug' => true,
    'url' => 'http://localhost:8000',
    'timezone' => 'UTC',
    'locale' => 'en_GB',
    'available_locales' => 
    array (
      0 => 'en_GB',
      1 => 'ru_RU',
      2 => 'ar_SA',
    ),
    'default_locale_for_lang' => 
    array (
      'en' => 'en_GB',
      'ru' => 'ru_RU',
      'ar' => 'ar_SA',
    ),
    'fallback_locale' => 'en_GB',
    'key' => 'base64:BEDKR0m7oHMArmnRxPMRlTyoMaSN8u784wxYC8D5WME=',
    'cipher' => 'AES-256-CBC',
    'log' => 'single',
    'log_level' => 'debug',
    'providers' => 
    array (
      0 => 'Illuminate\\Auth\\AuthServiceProvider',
      1 => 'Illuminate\\Broadcasting\\BroadcastServiceProvider',
      2 => 'Illuminate\\Bus\\BusServiceProvider',
      3 => 'Illuminate\\Cache\\CacheServiceProvider',
      4 => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
      5 => 'Illuminate\\Cookie\\CookieServiceProvider',
      6 => 'Illuminate\\Database\\DatabaseServiceProvider',
      7 => 'Illuminate\\Encryption\\EncryptionServiceProvider',
      8 => 'Illuminate\\Filesystem\\FilesystemServiceProvider',
      9 => 'Illuminate\\Foundation\\Providers\\FoundationServiceProvider',
      10 => 'Illuminate\\Hashing\\HashServiceProvider',
      11 => 'Illuminate\\Mail\\MailServiceProvider',
      12 => 'Illuminate\\Notifications\\NotificationServiceProvider',
      13 => 'Illuminate\\Pagination\\PaginationServiceProvider',
      14 => 'Illuminate\\Pipeline\\PipelineServiceProvider',
      15 => 'Illuminate\\Queue\\QueueServiceProvider',
      16 => 'Illuminate\\Redis\\RedisServiceProvider',
      17 => 'Illuminate\\Auth\\Passwords\\PasswordResetServiceProvider',
      18 => 'Illuminate\\Session\\SessionServiceProvider',
      19 => 'Illuminate\\Translation\\TranslationServiceProvider',
      20 => 'Illuminate\\Validation\\ValidationServiceProvider',
      21 => 'Illuminate\\View\\ViewServiceProvider',
      22 => 'Phaza\\LaravelPostgis\\DatabaseServiceProvider',
      23 => 'Cocur\\Slugify\\Bridge\\Laravel\\SlugifyServiceProvider',
      24 => 'Cviebrock\\EloquentSluggable\\ServiceProvider',
      25 => 'Collective\\Html\\HtmlServiceProvider',
      26 => 'Laracasts\\Flash\\FlashServiceProvider',
      27 => 'Prettus\\Repository\\Providers\\RepositoryServiceProvider',
      28 => 'InfyOm\\Generator\\InfyOmGeneratorServiceProvider',
      29 => 'InfyOm\\AdminLTETemplates\\AdminLTETemplatesServiceProvider',
      30 => 'Spatie\\Permission\\PermissionServiceProvider',
      31 => 'Spatie\\Fractal\\FractalServiceProvider',
      32 => 'App\\Providers\\AppServiceProvider',
      33 => 'App\\Providers\\AuthServiceProvider',
      34 => 'App\\Providers\\EventServiceProvider',
      35 => 'App\\Providers\\RouteServiceProvider',
      36 => 'App\\Providers\\MinIOStorageServiceProvider',
    ),
    'aliases' => 
    array (
      'App' => 'Illuminate\\Support\\Facades\\App',
      'Arr' => 'Illuminate\\Support\\Arr',
      'Artisan' => 'Illuminate\\Support\\Facades\\Artisan',
      'Auth' => 'Illuminate\\Support\\Facades\\Auth',
      'Blade' => 'Illuminate\\Support\\Facades\\Blade',
      'Bus' => 'Illuminate\\Support\\Facades\\Bus',
      'Cache' => 'Illuminate\\Support\\Facades\\Cache',
      'Config' => 'Illuminate\\Support\\Facades\\Config',
      'Cookie' => 'Illuminate\\Support\\Facades\\Cookie',
      'Crypt' => 'Illuminate\\Support\\Facades\\Crypt',
      'DB' => 'Illuminate\\Support\\Facades\\DB',
      'Eloquent' => 'Illuminate\\Database\\Eloquent\\Model',
      'Event' => 'Illuminate\\Support\\Facades\\Event',
      'File' => 'Illuminate\\Support\\Facades\\File',
      'Flash' => 'Laracasts\\Flash\\Flash',
      'Form' => 'Collective\\Html\\FormFacade',
      'Gate' => 'Illuminate\\Support\\Facades\\Gate',
      'Hash' => 'Illuminate\\Support\\Facades\\Hash',
      'Html' => 'Collective\\Html\\HtmlFacade',
      'Lang' => 'Illuminate\\Support\\Facades\\Lang',
      'Log' => 'Illuminate\\Support\\Facades\\Log',
      'Mail' => 'Illuminate\\Support\\Facades\\Mail',
      'Notification' => 'Illuminate\\Support\\Facades\\Notification',
      'Password' => 'Illuminate\\Support\\Facades\\Password',
      'Queue' => 'Illuminate\\Support\\Facades\\Queue',
      'Redirect' => 'Illuminate\\Support\\Facades\\Redirect',
      'Redis' => 'Illuminate\\Support\\Facades\\Redis',
      'Request' => 'Illuminate\\Support\\Facades\\Request',
      'Response' => 'Illuminate\\Support\\Facades\\Response',
      'Route' => 'Illuminate\\Support\\Facades\\Route',
      'Schema' => 'Illuminate\\Support\\Facades\\Schema',
      'Session' => 'Illuminate\\Support\\Facades\\Session',
      'Storage' => 'Illuminate\\Support\\Facades\\Storage',
      'URL' => 'Illuminate\\Support\\Facades\\URL',
      'Validator' => 'Illuminate\\Support\\Facades\\Validator',
      'View' => 'Illuminate\\Support\\Facades\\View',
      'Slugify' => 'Cocur\\Slugify\\Bridge\\Laravel\\SlugifyFacade',
    ),
  ),
  'auth' => 
  array (
    'defaults' => 
    array (
      'guard' => 'twill_users',
      'passwords' => 'users',
    ),
    'guards' => 
    array (
      'twill_users' => 
      array (
        'driver' => 'session',
        'provider' => 'users',
      ),
      'web' => 
      array (
        'driver' => 'session',
        'provider' => 'users',
      ),
      'api' => 
      array (
        'driver' => 'sanctum',
        'provider' => 'users',
      ),
      'sanctum' => 
      array (
        'driver' => 'sanctum',
        'provider' => NULL,
      ),
    ),
    'providers' => 
    array (
      'twill_users' => 
      array (
        'driver' => 'eloquent',
        'model' => 'App\\Models\\Users\\User',
      ),
      'users' => 
      array (
        'driver' => 'eloquent',
        'model' => 'App\\Models\\Users\\User',
      ),
    ),
    'passwords' => 
    array (
      'users' => 
      array (
        'provider' => 'users',
        'table' => 'password_resets',
        'expire' => 60,
      ),
    ),
  ),
  'broadcasting' => 
  array (
    'default' => 'log',
    'connections' => 
    array (
      'pusher' => 
      array (
        'driver' => 'pusher',
        'key' => '',
        'secret' => '',
        'app_id' => '',
        'options' => 
        array (
        ),
      ),
      'redis' => 
      array (
        'driver' => 'redis',
        'connection' => 'default',
      ),
      'log' => 
      array (
        'driver' => 'log',
      ),
      'null' => 
      array (
        'driver' => 'null',
      ),
    ),
  ),
  'cache' => 
  array (
    'default' => 'file',
    'stores' => 
    array (
      'apc' => 
      array (
        'driver' => 'apc',
      ),
      'array' => 
      array (
        'driver' => 'array',
      ),
      'database' => 
      array (
        'driver' => 'database',
        'table' => 'cache',
        'connection' => NULL,
      ),
      'file' => 
      array (
        'driver' => 'file',
        'path' => '/var/www/app/storage/framework/cache',
      ),
      'memcached' => 
      array (
        'driver' => 'memcached',
        'persistent_id' => NULL,
        'sasl' => 
        array (
          0 => NULL,
          1 => NULL,
        ),
        'options' => 
        array (
        ),
        'servers' => 
        array (
          0 => 
          array (
            'host' => '127.0.0.1',
            'port' => 11211,
            'weight' => 100,
          ),
        ),
      ),
      'redis' => 
      array (
        'driver' => 'redis',
        'connection' => 'default',
      ),
    ),
    'prefix' => 'laravel',
  ),
  'compile' => 
  array (
    'files' => 
    array (
    ),
    'providers' => 
    array (
    ),
  ),
  'database' => 
  array (
    'fetch' => 5,
    'default' => 'pgsql',
    'connections' => 
    array (
      'testing' => 
      array (
        'driver' => 'pgsql',
        'database' => ':memory:',
        'prefix' => '',
      ),
      'sqlite' => 
      array (
        'driver' => 'sqlite',
        'database' => 'laravel',
        'prefix' => '',
      ),
      'mysql' => 
      array (
        'driver' => 'mysql',
        'host' => 'db',
        'port' => '5432',
        'database' => 'laravel',
        'username' => 'laravel',
        'password' => 'laravel',
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix' => '',
        'strict' => true,
        'engine' => NULL,
      ),
      'pgsql' => 
      array (
        'driver' => 'pgsql',
        'host' => 'db',
        'port' => '5432',
        'database' => 'laravel',
        'username' => 'laravel',
        'password' => 'laravel',
        'charset' => 'utf8',
        'prefix' => '',
        'schema' => 'public',
        'sslmode' => 'prefer',
      ),
    ),
    'migrations' => 'migrations',
    'redis' => 
    array (
      'cluster' => false,
      'default' => 
      array (
        'host' => 'redis',
        'password' => NULL,
        'port' => '6379',
        'database' => 0,
      ),
    ),
  ),
  'doctrine' => 
  array (
    'managers' => 
    array (
      'default' => 
      array (
        'dev' => true,
        'meta' => 'annotations',
        'connection' => 'pgsql',
        'namespaces' => 
        array (
          0 => 'App',
        ),
        'paths' => 
        array (
          0 => '/var/www/app/app',
        ),
        'repository' => 'Doctrine\\ORM\\EntityRepository',
        'proxies' => 
        array (
          'namespace' => false,
          'path' => '/var/www/app/storage/proxies',
          'auto_generate' => false,
        ),
        'events' => 
        array (
          'listeners' => 
          array (
          ),
          'subscribers' => 
          array (
          ),
        ),
        'filters' => 
        array (
        ),
        'mapping_types' => 
        array (
          '_text' => 'text_array',
          '_int4' => 'int_array',
          'tsvector' => 'ts_vector',
          'tsquery' => 'ts_query',
          'xml' => 'xml',
          'inet' => 'inet',
          'macaddr' => 'macaddr',
        ),
      ),
    ),
    'extensions' => 
    array (
    ),
    'custom_types' => 
    array (
      'json' => 'LaravelDoctrine\\ORM\\Types\\Json',
      'geometry' => 'CrEOF\\Spatial\\DBAL\\Types\\GeometryType',
      'point' => 'CrEOF\\Spatial\\DBAL\\Types\\Geometry\\PointType',
      'polygon' => 'CrEOF\\Spatial\\DBAL\\Types\\Geometry\\PolygonType',
      'linestring' => 'CrEOF\\Spatial\\DBAL\\Types\\Geometry\\LineStringType',
      'geography' => 'CrEOF\\Spatial\\DBAL\\Types\\GeographyType',
      'text_array' => 'Doctrine\\DBAL\\PostgresTypes\\TextArrayType',
      'int_array' => 'Doctrine\\DBAL\\PostgresTypes\\IntArrayType',
      'ts_vector' => 'Doctrine\\DBAL\\PostgresTypes\\TsvectorType',
      'ts_query' => 'Doctrine\\DBAL\\PostgresTypes\\TsqueryType',
      'xml' => 'Doctrine\\DBAL\\PostgresTypes\\XmlType',
      'inet' => 'Doctrine\\DBAL\\PostgresTypes\\InetType',
      'macaddr' => 'Doctrine\\DBAL\\PostgresTypes\\MacAddrType',
    ),
    'custom_datetime_functions' => 
    array (
    ),
    'custom_numeric_functions' => 
    array (
      'st_contains' => 'CrEOF\\Spatial\\ORM\\Query\\AST\\Functions\\MySql\\STContains',
      'contains' => 'CrEOF\\Spatial\\ORM\\Query\\AST\\Functions\\MySql\\Contains',
      'st_area' => 'CrEOF\\Spatial\\ORM\\Query\\AST\\Functions\\MySql\\Area',
      'st_geomfromtext' => 'CrEOF\\Spatial\\ORM\\Query\\AST\\Functions\\MySql\\GeomFromText',
      'st_intersects' => 'CrEOF\\Spatial\\ORM\\Query\\AST\\Functions\\MySql\\STIntersects',
      'st_buffer' => 'CrEOF\\Spatial\\ORM\\Query\\AST\\Functions\\MySql\\STBuffer',
      'point' => 'CrEOF\\Spatial\\ORM\\Query\\AST\\Functions\\MySql\\Point',
    ),
    'custom_string_functions' => 
    array (
    ),
    'custom_hydration_modes' => 
    array (
    ),
    'logger' => false,
    'cache' => 
    array (
      'default' => 'array',
      'namespace' => NULL,
      'second_level' => false,
    ),
    'gedmo' => 
    array (
      'all_mappings' => false,
    ),
    'doctrine_presence_verifier' => false,
    'notifications' => 
    array (
      'channel' => 'database',
    ),
  ),
  'elfinder' => 
  array (
    'dir' => 
    array (
      0 => 'files',
    ),
    'disks' => 
    array (
      0 => 'local',
    ),
    'route' => 
    array (
      'prefix' => 'elfinder',
    ),
    'access' => 'Barryvdh\\Elfinder\\Elfinder::checkAccess',
    'roots' => 
    array (
      0 => 
      array (
        'driver' => 'LocalFileSystem',
        'path' => 'files/',
        'accessControl' => 'Serverfireteam\\Panel\\libs\\AppHelper::access',
        'URL' => 'http://localhost:8000/files',
        'uploadAllow' => 
        array (
          0 => 'image/png',
          1 => 'image/jpeg',
          2 => 'image/pjpeg',
          3 => 'image/gif',
        ),
        'uploadDeny' => 
        array (
          0 => 'all',
        ),
        'uploadOrder' => 
        array (
          0 => 'deny',
          1 => 'allow',
        ),
        'acceptedName' => 'Serverfireteam\\Panel\\libs\\AppHelper::validName',
      ),
    ),
    'options' => 
    array (
    ),
  ),
  'excel' => 
  array (
    'exports' => 
    array (
      'chunk_size' => 1000,
      'pre_calculate_formulas' => false,
      'strict_null_comparison' => false,
      'csv' => 
      array (
        'delimiter' => ',',
        'enclosure' => '"',
        'line_ending' => '
',
        'use_bom' => false,
        'include_separator_line' => false,
        'excel_compatibility' => false,
      ),
      'properties' => 
      array (
        'creator' => '',
        'lastModifiedBy' => '',
        'title' => '',
        'description' => '',
        'subject' => '',
        'keywords' => '',
        'category' => '',
        'manager' => '',
        'company' => '',
      ),
    ),
    'imports' => 
    array (
      'read_only' => true,
      'ignore_empty' => false,
      'heading_row' => 
      array (
        'formatter' => 'slug',
      ),
      'csv' => 
      array (
        'delimiter' => ',',
        'enclosure' => '"',
        'escape_character' => '\\',
        'contiguous' => false,
        'input_encoding' => 'UTF-8',
      ),
      'properties' => 
      array (
        'creator' => '',
        'lastModifiedBy' => '',
        'title' => '',
        'description' => '',
        'subject' => '',
        'keywords' => '',
        'category' => '',
        'manager' => '',
        'company' => '',
      ),
    ),
    'extension_detector' => 
    array (
      'xlsx' => 'Xlsx',
      'xlsm' => 'Xlsx',
      'xltx' => 'Xlsx',
      'xltm' => 'Xlsx',
      'xls' => 'Xls',
      'xlt' => 'Xls',
      'ods' => 'Ods',
      'ots' => 'Ods',
      'slk' => 'Slk',
      'xml' => 'Xml',
      'gnumeric' => 'Gnumeric',
      'htm' => 'Html',
      'html' => 'Html',
      'csv' => 'Csv',
      'tsv' => 'Csv',
      'pdf' => 'Dompdf',
    ),
    'value_binder' => 
    array (
      'default' => 'Maatwebsite\\Excel\\DefaultValueBinder',
    ),
    'cache' => 
    array (
      'enable' => true,
      'driver' => 'memory',
      'settings' => 
      array (
        'memoryCacheSize' => '32MB',
        'cacheTime' => 600,
      ),
      'memcache' => 
      array (
        'host' => 'localhost',
        'port' => 11211,
      ),
      'dir' => '/var/www/app/storage/cache',
    ),
    'transactions' => 
    array (
      'handler' => 'db',
    ),
    'temporary_files' => 
    array (
      'local_path' => '/var/www/app/storage/framework/laravel-excel',
      'remote_disk' => NULL,
      'remote_prefix' => NULL,
      'force_resync_remote' => NULL,
    ),
    'properties' => 
    array (
      'creator' => 'Maatwebsite',
      'lastModifiedBy' => 'Maatwebsite',
      'title' => 'Spreadsheet',
      'description' => 'Default spreadsheet export',
      'subject' => 'Spreadsheet export',
      'keywords' => 'maatwebsite, excel, export',
      'category' => 'Excel',
      'manager' => 'Maatwebsite',
      'company' => 'Maatwebsite',
    ),
    'sheets' => 
    array (
      'pageSetup' => 
      array (
        'orientation' => 'portrait',
        'paperSize' => '9',
        'scale' => '100',
        'fitToPage' => false,
        'fitToHeight' => true,
        'fitToWidth' => true,
        'columnsToRepeatAtLeft' => 
        array (
          0 => '',
          1 => '',
        ),
        'rowsToRepeatAtTop' => 
        array (
          0 => 0,
          1 => 0,
        ),
        'horizontalCentered' => false,
        'verticalCentered' => false,
        'printArea' => NULL,
        'firstPageNumber' => NULL,
      ),
    ),
    'creator' => 'Maatwebsite',
    'csv' => 
    array (
      'delimiter' => ',',
      'enclosure' => '"',
      'line_ending' => '
',
    ),
    'export' => 
    array (
      'autosize' => true,
      'generate_heading_by_indices' => true,
      'merged_cell_alignment' => 'left',
      'calculate' => false,
      'includeCharts' => false,
      'sheets' => 
      array (
        'page_margin' => false,
        'nullValue' => NULL,
        'startCell' => 'A1',
        'strictNullComparison' => false,
      ),
      'store' => 
      array (
        'path' => '/var/www/app/storage/exports',
        'returnInfo' => false,
      ),
      'pdf' => 
      array (
        'driver' => 'DomPDF',
        'drivers' => 
        array (
          'DomPDF' => 
          array (
            'path' => '/var/www/app/vendor/dompdf/dompdf/',
          ),
          'tcPDF' => 
          array (
            'path' => '/var/www/app/vendor/tecnick.com/tcpdf/',
          ),
          'mPDF' => 
          array (
            'path' => '/var/www/app/vendor/mpdf/mpdf/',
          ),
        ),
      ),
    ),
    'filters' => 
    array (
      'registered' => 
      array (
        'chunk' => 'Maatwebsite\\Excel\\Filters\\ChunkReadFilter',
      ),
      'enabled' => 
      array (
      ),
    ),
    'import' => 
    array (
      'heading' => 'slugged',
      'startRow' => 1,
      'separator' => '_',
      'includeCharts' => false,
      'to_ascii' => true,
      'encoding' => 
      array (
        'input' => 'UTF-8',
        'output' => 'UTF-8',
      ),
      'calculate' => true,
      'ignoreEmpty' => false,
      'force_sheets_collection' => false,
      'dates' => 
      array (
        'enabled' => true,
        'format' => false,
        'columns' => 
        array (
        ),
      ),
      'sheets' => 
      array (
        'test' => 
        array (
          'firstname' => 'A2',
        ),
      ),
    ),
    'views' => 
    array (
      'styles' => 
      array (
        'th' => 
        array (
          'font' => 
          array (
            'bold' => true,
            'size' => 12,
          ),
        ),
        'strong' => 
        array (
          'font' => 
          array (
            'bold' => true,
            'size' => 12,
          ),
        ),
        'b' => 
        array (
          'font' => 
          array (
            'bold' => true,
            'size' => 12,
          ),
        ),
        'i' => 
        array (
          'font' => 
          array (
            'italic' => true,
            'size' => 12,
          ),
        ),
        'h1' => 
        array (
          'font' => 
          array (
            'bold' => true,
            'size' => 24,
          ),
        ),
        'h2' => 
        array (
          'font' => 
          array (
            'bold' => true,
            'size' => 18,
          ),
        ),
        'h3' => 
        array (
          'font' => 
          array (
            'bold' => true,
            'size' => 13.5,
          ),
        ),
        'h4' => 
        array (
          'font' => 
          array (
            'bold' => true,
            'size' => 12,
          ),
        ),
        'h5' => 
        array (
          'font' => 
          array (
            'bold' => true,
            'size' => 10,
          ),
        ),
        'h6' => 
        array (
          'font' => 
          array (
            'bold' => true,
            'size' => 7.5,
          ),
        ),
        'a' => 
        array (
          'font' => 
          array (
            'underline' => true,
            'color' => 
            array (
              'argb' => 'FF0000FF',
            ),
          ),
        ),
        'hr' => 
        array (
          'borders' => 
          array (
            'bottom' => 
            array (
              'style' => 'thin',
              'color' => 
              array (
                0 => 'FF000000',
              ),
            ),
          ),
        ),
      ),
    ),
  ),
  'filesystems' => 
  array (
    'default' => 'local',
    'cloud' => 'minio',
    'disks' => 
    array (
      'twill_media_library' => 
      array (
        'driver' => 's3',
        'key' => NULL,
        'secret' => NULL,
        'region' => 'us-east-1',
        'bucket' => NULL,
        'root' => '',
        'use_https' => true,
        'endpoint' => NULL,
      ),
      'twill_file_library' => 
      array (
        'driver' => 's3',
        'key' => NULL,
        'secret' => NULL,
        'region' => 'us-east-1',
        'bucket' => NULL,
        'root' => '',
        'use_https' => true,
        'endpoint' => NULL,
      ),
      'local' => 
      array (
        'driver' => 'local',
        'root' => '/var/www/app/storage/app',
      ),
      'public' => 
      array (
        'driver' => 'local',
        'root' => '/var/www/app/storage/app/public',
        'visibility' => 'public',
      ),
      's3' => 
      array (
        'driver' => 's3',
        'key' => 'your-key',
        'secret' => 'your-secret',
        'region' => 'your-region',
        'bucket' => 'your-bucket',
      ),
      'minio' => 
      array (
        'driver' => 'minio',
        'key' => 'AKIAI4ANSDF2FDJ2ARA',
        'secret' => '+dFdjklfd23jskAzjLSfjkfgh5sSHFQZhWpuqp',
        'region' => 'us-east-1',
        'bucket' => 'fat-orp-simulations',
        'endpoint' => 'https://mo.ev.openindustry.in',
      ),
      'twill-s3' => 
      array (
        'driver' => 's3',
        'key' => 'AKIAI4ANSDF2FDJ2ARA',
        'secret' => '+dFdjklfd23jskAzjLSfjkfgh5sSHFQZhWpuqp',
        'region' => 'us-east-1',
        'bucket' => 'fat-orp-simulations',
        'url' => 'https://mo.ev.openindustry.in',
        'endpoint' => 'https://mo.ev.openindustry.in/fat-orp-simulations',
        'use_path_style_endpoint' => true,
        'cache' => 
        array (
          'store' => 'redis',
          'expire' => 600,
          'prefix' => 'filesystem-s3-cache',
        ),
      ),
    ),
  ),
  'flysystem' => 
  array (
    'default' => 'minio',
    'connections' => 
    array (
      'awss3' => 
      array (
        'driver' => 'awss3',
        'key' => 'your-key',
        'secret' => 'your-secret',
        'bucket' => 'your-bucket',
        'region' => 'your-region',
        'version' => 'latest',
      ),
      'azure' => 
      array (
        'driver' => 'azure',
        'account-name' => 'your-account-name',
        'api-key' => 'your-api-key',
        'container' => 'your-container',
      ),
      'dropbox' => 
      array (
        'driver' => 'dropbox',
        'token' => 'your-token',
      ),
      'ftp' => 
      array (
        'driver' => 'ftp',
        'host' => 'ftp.example.com',
        'port' => 21,
        'username' => 'your-username',
        'password' => 'your-password',
      ),
      'gridfs' => 
      array (
        'driver' => 'gridfs',
        'server' => 'mongodb://localhost:27017',
        'database' => 'your-database',
      ),
      'local' => 
      array (
        'driver' => 'local',
        'path' => '/var/www/app/storage/files',
      ),
      'null' => 
      array (
        'driver' => 'null',
      ),
      'rackspace' => 
      array (
        'driver' => 'rackspace',
        'endpoint' => 'your-endpoint',
        'region' => 'your-region',
        'username' => 'your-username',
        'apiKey' => 'your-api-key',
        'container' => 'your-container',
      ),
      'replicate' => 
      array (
        'driver' => 'replicate',
        'source' => 'your-source-adapter',
        'replica' => 'your-replica-adapter',
      ),
      'sftp' => 
      array (
        'driver' => 'sftp',
        'host' => 'sftp.example.com',
        'port' => 22,
        'username' => 'your-username',
        'password' => 'your-password',
      ),
      'webdav' => 
      array (
        'driver' => 'webdav',
        'baseUri' => 'http://example.org/dav/',
        'userName' => 'your-username',
        'password' => 'your-password',
      ),
      'zip' => 
      array (
        'driver' => 'zip',
        'path' => '/var/www/app/storage/files.zip',
      ),
      'minio' => 
      array (
        'driver' => 'minio',
        'key' => 'AKIAI4ANSDF2FDJ2ARA',
        'secret' => '+dFdjklfd23jskAzjLSfjkfgh5sSHFQZhWpuqp',
        'region' => 'us-east-1',
        'bucket' => 'fat-orp-simulations',
        'endpoint' => 'https://mo.ev.openindustry.in',
      ),
    ),
    'cache' => 
    array (
      'foo' => 
      array (
        'driver' => 'illuminate',
        'connector' => NULL,
        'key' => 'foo',
      ),
      'bar' => 
      array (
        'driver' => 'illuminate',
        'connector' => 'redis',
        'key' => 'bar',
        'ttl' => 600,
      ),
      'adapter' => 
      array (
        'driver' => 'adapter',
        'adapter' => 'local',
        'file' => 'flysystem.json',
        'ttl' => 600,
      ),
    ),
  ),
  'fractal' => 
  array (
    'default_serializer' => 'League\\Fractal\\Serializer\\JsonApiSerializer',
    'default_paginator' => '',
    'base_url' => NULL,
    'fractal_class' => 'Spatie\\Fractal\\Fractal',
    'auto_includes' => 
    array (
      'enabled' => true,
      'request_key' => 'include',
    ),
  ),
  'infyom' => 
  array (
    'laravel_generator' => 
    array (
      'path' => 
      array (
        'migration' => '/var/www/app/database/migrations/',
        'model' => '/var/www/app/app/Models/',
        'datatables' => '/var/www/app/app/DataTables/',
        'repository' => '/var/www/app/app/Repositories/',
        'routes' => '/var/www/app/routes/web.php',
        'api_routes' => '/var/www/app/routes/api.php',
        'request' => '/var/www/app/app/Http/Requests/',
        'api_request' => '/var/www/app/app/Http/Requests/API/',
        'controller' => '/var/www/app/app/Http/Controllers/',
        'api_controller' => '/var/www/app/app/Http/Controllers/API/',
        'test_trait' => '/var/www/app/tests/traits/',
        'repository_test' => '/var/www/app/tests/',
        'api_test' => '/var/www/app/tests/',
        'views' => '/var/www/app/resources/views/',
        'schema_files' => '/var/www/app/resources/model_schemas/',
        'templates_dir' => '/var/www/app/resources/infyom/infyom-generator-templates/',
        'modelJs' => '/var/www/app/resources/assets/js/models/',
      ),
      'namespace' => 
      array (
        'model' => 'App\\Models',
        'datatables' => 'App\\DataTables',
        'repository' => 'App\\Repositories',
        'controller' => 'App\\Http\\Controllers',
        'api_controller' => 'App\\Http\\Controllers\\API',
        'request' => 'App\\Http\\Requests',
        'api_request' => 'App\\Http\\Requests\\API',
      ),
      'templates' => 'adminlte-templates',
      'model_extend_class' => 'Eloquent',
      'api_prefix' => 'api',
      'api_version' => 'v1',
      'options' => 
      array (
        'softDelete' => false,
        'tables_searchable_default' => false,
        'uuidModel' => true,
        'usePopulator' => true,
        'routeFormat' => 'dashed',
      ),
      'prefixes' => 
      array (
        'route' => '',
        'path' => '',
        'view' => '',
        'public' => '',
      ),
      'add_on' => 
      array (
        'swagger' => true,
        'tests' => true,
        'datatables' => false,
        'menu' => 
        array (
          'enabled' => true,
          'menu_file' => 'layouts/menu.blade.php',
        ),
      ),
      'timestamps' => 
      array (
        'enabled' => true,
        'created_at' => 'created_at',
        'updated_at' => 'updated_at',
        'deleted_at' => 'deleted_at',
      ),
    ),
  ),
  'laravel-gettext' => 
  array (
    'handler' => 'symfony',
    'session-identifier' => 'laravel-gettext-locale',
    'locale' => 'en',
    'supported-locales' => 
    array (
      0 => 'en_GB',
      1 => 'ru',
      2 => 'ar_SA',
    ),
    'encoding' => 'UTF-8',
    'categories' => 
    array (
      0 => 'LC_ALL',
    ),
    'translations-path' => '../resources/openaugury-translations/resources/lang/',
    'relative-path' => '../../../../../app',
    'fallback-locale' => 'en_GB',
    'domain' => 'lang',
    'project' => 'MultilanguageLaravelApplication',
    'translator' => 'James Translator <james@translations.colm>',
    'source-paths' => 
    array (
      0 => 'Http',
      1 => '../resources/views',
      2 => 'Console',
    ),
    'sync-laravel' => true,
    'adapter' => 'Xinax\\LaravelGettext\\Adapters\\LaravelAdapter',
    'storage' => 'Xinax\\LaravelGettext\\Storages\\SessionStorage',
    'custom-locale' => false,
    'keywords-list' => 
    array (
      0 => '_',
      1 => '__',
      2 => '_i',
      3 => '_s',
      4 => 'gettext',
      5 => '_n:1,2',
      6 => 'ngettext:1,2',
      7 => 'dgettext:2',
    ),
  ),
  'laravel-permission' => 
  array (
    'models' => 
    array (
      'permission' => 'Spatie\\Permission\\Models\\Permission',
      'role' => 'Spatie\\Permission\\Models\\Role',
    ),
    'table_names' => 
    array (
      'users' => 'users',
      'roles' => 'roles',
      'permissions' => 'permissions',
      'user_has_permissions' => 'user_has_permissions',
      'user_has_roles' => 'user_has_roles',
      'role_has_permissions' => 'role_has_permissions',
    ),
    'foreign_keys' => 
    array (
      'users' => 'user_id',
    ),
    'log_registration_exception' => true,
  ),
  'logging' => 
  array (
    'default' => 'errorlog',
    'channels' => 
    array (
      'stack' => 
      array (
        'driver' => 'stack',
        'channels' => 
        array (
          0 => 'daily',
        ),
        'ignore_exceptions' => false,
      ),
      'single' => 
      array (
        'driver' => 'single',
        'path' => '/var/www/app/storage/logs/laravel.log',
        'level' => 'debug',
      ),
      'daily' => 
      array (
        'driver' => 'daily',
        'path' => '/var/www/app/storage/logs/laravel.log',
        'level' => 'debug',
        'days' => 14,
      ),
      'slack' => 
      array (
        'driver' => 'slack',
        'url' => NULL,
        'username' => 'Laravel Log',
        'emoji' => ':boom:',
        'level' => 'critical',
      ),
      'papertrail' => 
      array (
        'driver' => 'monolog',
        'level' => 'debug',
        'handler' => 'Monolog\\Handler\\SyslogUdpHandler',
        'handler_with' => 
        array (
          'host' => NULL,
          'port' => NULL,
        ),
      ),
      'stderr' => 
      array (
        'driver' => 'monolog',
        'handler' => 'Monolog\\Handler\\StreamHandler',
        'formatter' => NULL,
        'with' => 
        array (
          'stream' => 'php://stderr',
        ),
      ),
      'syslog' => 
      array (
        'driver' => 'syslog',
        'level' => 'debug',
      ),
      'errorlog' => 
      array (
        'driver' => 'errorlog',
        'level' => 'debug',
      ),
    ),
  ),
  'mail' => 
  array (
    'driver' => 'smtp',
    'host' => 'mailtrap.io',
    'port' => '2525',
    'from' => 
    array (
      'address' => 'hello@example.com',
      'name' => 'Example',
    ),
    'encryption' => NULL,
    'username' => NULL,
    'password' => NULL,
    'sendmail' => '/usr/sbin/sendmail -bs',
  ),
  'openaugury' => 
  array (
    'anonymous-access' => true,
    'brand' => 
    array (
      'slug' => 'openaugury',
      'name' => 'OpenAugury',
      'logo' => 
      array (
        'long' => '/images/logo-text.png',
        'icon' => '/images/logo.png',
      ),
      'splash' => 
      array (
        'url-default' => '/images/splash.jpg',
        'url-small' => '/images/splash-small.jpg',
        'attribution-text' => 'Image attribution',
        'attribution-url' => 'http://example.com',
      ),
    ),
    'simulation' => 
    array (
      'server' => 'http://function',
      'port' => '5000',
      'maximum_decode_memory' => '1000M',
    ),
    'legal' => 
    array (
      'terms-and-conditions-link' => 'https://s3.eu-west-2.amazonaws.com/ourragingplanet-media/subscription-terms-conditions.pdf',
      'privacy-policy-link' => 'https://s3.eu-west-2.amazonaws.com/ourragingplanet-media/privacy-and-cookies-policy.pdf',
    ),
    'frontend' => 
    array (
      'proxy' => 'http://172.24.0.1:8180/',
      'prefix' => 'static/',
      'allow-http' => '1',
    ),
  ),
  'panel' => 
  array (
    'panelControllers' => 
    array (
      0 => 'Admin',
      1 => 'Permission',
      2 => 'Role',
      3 => 'Link',
    ),
    'logo' => 'packages/serverfireteam/panel/img/logo.png',
    'modelPath' => 'Models',
    'adminUserClass' => 'App\\Models\\Users\\User',
  ),
  'queue' => 
  array (
    'default' => 'redis',
    'connections' => 
    array (
      'sync' => 
      array (
        'driver' => 'sync',
      ),
      'database' => 
      array (
        'driver' => 'database',
        'table' => 'jobs',
        'queue' => 'default',
        'retry_after' => 90,
      ),
      'beanstalkd' => 
      array (
        'driver' => 'beanstalkd',
        'host' => 'localhost',
        'queue' => 'default',
        'retry_after' => 90,
      ),
      'sqs' => 
      array (
        'driver' => 'sqs',
        'key' => 'your-public-key',
        'secret' => 'your-secret-key',
        'prefix' => 'https://sqs.us-east-1.amazonaws.com/your-account-id',
        'queue' => 'your-queue-name',
        'region' => 'us-east-1',
      ),
      'redis' => 
      array (
        'driver' => 'redis',
        'connection' => 'default',
        'queue' => 'default',
        'retry_after' => 90,
      ),
    ),
    'failed' => 
    array (
      'database' => 'pgsql',
      'table' => 'failed_jobs',
    ),
  ),
  'rapyd' => 
  array (
    'data_edit' => 
    array (
      'button_position' => 
      array (
        'save' => 'BL',
        'show' => 'TR',
        'modify' => 'TR',
        'undo' => 'TR',
        'delete' => 'BL',
      ),
    ),
    'sanitize' => 
    array (
      'num_characters' => 100,
    ),
    'fields' => 
    array (
      'attributes' => 
      array (
        'class' => 'form-control',
      ),
      'date' => 
      array (
        'format' => 'm/d/Y',
      ),
      'datetime' => 
      array (
        'format' => 'm/d/Y H:i:s',
        'store_as' => 'Y-m-d H:i:s',
      ),
    ),
    'tinymce' => 
    array (
      'language' => false,
    ),
  ),
  'repository' => 
  array (
    'pagination' => 
    array (
      'limit' => 15,
    ),
    'fractal' => 
    array (
      'params' => 
      array (
        'include' => 'include',
      ),
      'serializer' => 'League\\Fractal\\Serializer\\DataArraySerializer',
    ),
    'cache' => 
    array (
      'enabled' => true,
      'minutes' => 30,
      'repository' => 'cache',
      'clean' => 
      array (
        'enabled' => true,
        'on' => 
        array (
          'create' => true,
          'update' => true,
          'delete' => true,
        ),
      ),
      'params' => 
      array (
        'skipCache' => 'skipCache',
      ),
      'allowed' => 
      array (
        'only' => NULL,
        'except' => NULL,
      ),
    ),
    'criteria' => 
    array (
      'acceptedConditions' => 
      array (
        0 => '=',
        1 => 'like',
      ),
      'params' => 
      array (
        'search' => 'search',
        'searchFields' => 'searchFields',
        'filter' => 'filter',
        'orderBy' => 'orderBy',
        'sortedBy' => 'sortedBy',
        'with' => 'with',
      ),
    ),
    'generator' => 
    array (
      'basePath' => '/var/www/app/app',
      'rootNamespace' => 'App\\',
      'paths' => 
      array (
        'models' => 'Entities',
        'repositories' => 'Repositories',
        'interfaces' => 'Repositories',
        'transformers' => 'Transformers',
        'presenters' => 'Presenters',
        'validators' => 'Validators',
        'controllers' => 'Http/Controllers',
        'provider' => 'RepositoryServiceProvider',
        'criteria' => 'Criteria',
        'stubsOverridePath' => '/var/www/app/app',
      ),
    ),
  ),
  'sanctum' => 
  array (
    'stateful' => 
    array (
      0 => 'localhost:8000',
      1 => 'localhost',
      2 => '127.0.0.1',
    ),
    'expiration' => NULL,
    'middleware' => 
    array (
      'verify_csrf_token' => 'App\\Http\\Middleware\\VerifyCsrfToken',
    ),
    'guard' => 'twill_users',
  ),
  'services' => 
  array (
    'cloudfront' => 
    array (
      'key' => NULL,
      'secret' => NULL,
      'distribution' => NULL,
      'sdk_version' => '2017-10-30',
      'region' => 'us-east-1',
    ),
    'github' => 
    array (
      'client_id' => NULL,
      'client_secret' => NULL,
      'redirect' => '/login/oauth/callback/github',
    ),
    'google' => 
    array (
      'client_id' => NULL,
      'client_secret' => NULL,
      'redirect' => '/login/oauth/callback/google',
    ),
    'facebook' => 
    array (
      'client_id' => NULL,
      'client_secret' => NULL,
      'redirect' => '/login/oauth/callback/facebook',
    ),
    'twitter' => 
    array (
      'client_id' => NULL,
      'client_secret' => NULL,
      'redirect' => '/login/oauth/callback/twitter',
    ),
    'linkedin' => 
    array (
      'client_id' => NULL,
      'client_secret' => NULL,
      'redirect' => '/login/oauth/callback/linkedin',
    ),
    'gitlab' => 
    array (
      'client_id' => NULL,
      'client_secret' => NULL,
      'redirect' => '/login/oauth/callback/gitlab',
    ),
    'bitbucket' => 
    array (
      'client_id' => NULL,
      'client_secret' => NULL,
      'redirect' => '/login/oauth/callback/bitbucket',
    ),
    'mailgun' => 
    array (
      'domain' => NULL,
      'secret' => NULL,
    ),
    'ses' => 
    array (
      'key' => NULL,
      'secret' => NULL,
      'region' => 'us-east-1',
    ),
    'sparkpost' => 
    array (
      'secret' => NULL,
    ),
    'stripe' => 
    array (
      'model' => 'App\\User',
      'key' => NULL,
      'secret' => NULL,
    ),
  ),
  'session' => 
  array (
    'driver' => 'file',
    'lifetime' => 120,
    'expire_on_close' => false,
    'encrypt' => false,
    'files' => '/var/www/app/storage/framework/sessions',
    'connection' => NULL,
    'table' => 'sessions',
    'store' => NULL,
    'lottery' => 
    array (
      0 => 2,
      1 => 100,
    ),
    'cookie' => 'laravel_session',
    'path' => '/',
    'domain' => 'localhost',
    'secure' => false,
    'http_only' => true,
  ),
  'sluggable' => 
  array (
    'source' => NULL,
    'maxLength' => NULL,
    'maxLengthKeepWords' => true,
    'method' => NULL,
    'separator' => '-',
    'unique' => true,
    'uniqueSuffix' => NULL,
    'includeTrashed' => false,
    'reserved' => NULL,
    'onUpdate' => false,
  ),
  'translatable' => 
  array (
    'locales' => 
    array (
      0 => 'en_GB',
      1 => '',
    ),
    'locale_separator' => '_',
    'locale' => NULL,
    'use_fallback' => false,
    'use_property_fallback' => true,
    'fallback_locale' => 'en_GB',
    'translation_model_namespace' => NULL,
    'translation_suffix' => 'Translation',
    'locale_key' => 'locale',
    'to_array_always_loads_translations' => true,
    'rule_factory' => 
    array (
      'format' => 1,
      'prefix' => '%',
      'suffix' => '%',
    ),
    'available_locales' => 
    array (
      0 => 'en_GB',
      1 => 'ru_RU',
      2 => 'ar_SA',
    ),
  ),
  'twill' => 
  array (
    'namespace' => 'App',
    'admin_app_url' => 'localhost',
    'admin_app_path' => 'admin',
    'admin_app_title_suffix' => 'Admin',
    'support_subdomain_admin_routing' => false,
    'admin_app_subdomain' => 'admin',
    'active_subdomain' => NULL,
    'admin_route_patterns' => 
    array (
    ),
    'allow_duplicates_on_route_names' => true,
    'admin_middleware_group' => 'web',
    'users_table' => 'users',
    'password_resets_table' => 'twill_password_resets',
    'users_oauth_table' => 'twill_users_oauth',
    'blocks_table' => 'blocks',
    'features_table' => 'features',
    'settings_table' => 'settings',
    'medias_table' => 'medias',
    'mediables_table' => 'mediables',
    'files_table' => 'files',
    'fileables_table' => 'fileables',
    'related_table' => 'related',
    'migrations_use_big_integers' => true,
    'load_default_migrations' => true,
    'auth_login_redirect_path' => '/admin',
    'templates_on_frontend_domain' => false,
    'google_maps_api_key' => NULL,
    'js_namespace' => 'TWILL',
    'dev_mode' => false,
    'dev_mode_url' => 'http://localhost:8080',
    'public_directory' => 'assets/admin',
    'manifest_file' => 'twill-manifest.json',
    'vendor_path' => 'vendor/area17/twill',
    'custom_components_resource_path' => 'assets/js/components/admin',
    'build_timeout' => 300,
    'internal_icons' => 
    array (
      0 => 'content-editor.svg',
      1 => 'close_modal.svg',
      2 => 'edit_large.svg',
      3 => 'google-sign-in.svg',
    ),
    'locale' => 'en',
    'fallback_locale' => 'en',
    'available_user_locales' => 
    array (
      0 => 'en_GB',
      1 => 'ru_RU',
      2 => 'ar_SA',
    ),
    'enabled' => 
    array (
      'users-management' => false,
      'media-library' => true,
      'file-library' => true,
      'block-editor' => true,
      'buckets' => true,
      'users-image' => false,
      'site-link' => false,
      'settings' => true,
      'dashboard' => true,
      'search' => true,
      'users-description' => false,
      'activitylog' => false,
      'users-2fa' => false,
      'users-oauth' => false,
    ),
    'file_library' => 
    array (
      'disk' => 'twill-s3',
      'endpoint_type' => 's3',
      'cascade_delete' => true,
      'local_path' => 'uploads',
      'file_service' => 'A17\\Twill\\Services\\FileLibrary\\Disk',
      'acl' => 'public-read',
      'filesize_limit' => 50,
      'allowed_extensions' => 
      array (
        0 => 'pdf',
        1 => 'txt',
        2 => 'doc',
      ),
      'prefix_uuid_with_local_path' => false,
    ),
    'block_editor' => 
    array (
      'block_single_layout' => 'site.layouts.block',
      'block_views_path' => 'site.blocks',
      'block_views_mappings' => 
      array (
      ),
      'block_preview_render_childs' => true,
      'block_presenter_path' => NULL,
      'inline_blocks_templates' => true,
      'custom_vue_blocks_resource_path' => 'assets/js/blocks',
      'use_twill_blocks' => 
      array (
        0 => 'text',
        1 => 'image',
      ),
      'crops' => 
      array (
        'image' => 
        array (
          'desktop' => 
          array (
            0 => 
            array (
              'name' => 'desktop',
              'ratio' => 1.7777777777777777,
              'minValues' => 
              array (
                'width' => 100,
                'height' => 100,
              ),
            ),
          ),
          'tablet' => 
          array (
            0 => 
            array (
              'name' => 'tablet',
              'ratio' => 1.3333333333333333,
              'minValues' => 
              array (
                'width' => 100,
                'height' => 100,
              ),
            ),
          ),
          'mobile' => 
          array (
            0 => 
            array (
              'name' => 'mobile',
              'ratio' => 1,
              'minValues' => 
              array (
                'width' => 100,
                'height' => 100,
              ),
            ),
          ),
        ),
      ),
      'repeaters' => 
      array (
      ),
      'directories' => 
      array (
        'source' => 
        array (
          'blocks' => 
          array (
            0 => 
            array (
              'path' => '/var/www/app/vendor/area17/twill/src/Commands/stubs/blocks',
              'source' => 'twill',
            ),
            1 => 
            array (
              'path' => '/var/www/app/resources/views/admin/blocks',
              'source' => 'app',
            ),
          ),
          'repeaters' => 
          array (
            0 => 
            array (
              'path' => '/var/www/app/resources/views/admin/repeaters',
              'source' => 'app',
            ),
            1 => 
            array (
              'path' => '/var/www/app/vendor/area17/twill/src/Commands/stubs/repeaters',
              'source' => 'twill',
            ),
          ),
          'icons' => 
          array (
            0 => '/var/www/app/vendor/area17/twill/frontend/icons',
            1 => '/var/www/app/resources/views/admin/icons',
          ),
        ),
        'destination' => 
        array (
          'make_dir' => true,
          'blocks' => '/var/www/app/resources/views/admin/blocks',
          'repeaters' => '/var/www/app/resources/views/admin/repeaters',
        ),
      ),
      'files' => 
      array (
        0 => 'newsFile',
        1 => 'taskFile',
      ),
    ),
    'frontend' => 
    array (
      'rev_manifest_path' => '/var/www/app/public/dist/rev-manifest.json',
      'dev_assets_path' => '/dist',
      'dist_assets_path' => '/dist',
      'svg_sprites_path' => 'sprites.svg',
      'svg_sprites_use_hash_only' => true,
      'views_path' => 'site',
      'home_route_name' => 'home',
    ),
    'debug' => 
    array (
    ),
    'seo' => 
    array (
      'site_title' => 'OpenAugury',
      'site_desc' => 'OpenAugury',
      'image_default_id' => NULL,
      'image_local_fallback' => NULL,
    ),
    'media_library' => 
    array (
      'disk' => 'twill_media_library',
      'endpoint_type' => 's3',
      'cascade_delete' => false,
      'local_path' => 'uploads',
      'image_service' => 'A17\\Twill\\Services\\MediaLibrary\\Imgix',
      'acl' => 'private',
      'filesize_limit' => 50,
      'allowed_extensions' => 
      array (
        0 => 'svg',
        1 => 'jpg',
        2 => 'gif',
        3 => 'png',
        4 => 'jpeg',
      ),
      'init_alt_text_from_filename' => true,
      'prefix_uuid_with_local_path' => false,
      'translated_form_fields' => false,
      'show_file_name' => false,
    ),
    'imgix' => 
    array (
      'source_host' => NULL,
      'use_https' => true,
      'use_signed_urls' => false,
      'sign_key' => NULL,
      'add_params_to_svgs' => false,
      'default_params' => 
      array (
        'fm' => 'jpg',
        'q' => '80',
        'auto' => 'compress,format',
        'fit' => 'min',
      ),
      'lqip_default_params' => 
      array (
        'fm' => 'gif',
        'auto' => 'compress',
        'blur' => 100,
        'dpr' => 1,
      ),
      'social_default_params' => 
      array (
        'fm' => 'jpg',
        'w' => 900,
        'h' => 470,
        'fit' => 'crop',
        'crop' => 'entropy',
      ),
      'cms_default_params' => 
      array (
        'q' => 60,
        'dpr' => 1,
      ),
    ),
    'glide' => 
    array (
      'source' => '/var/www/app/storage/app/public/uploads',
      'cache' => '/var/www/app/storage/app',
      'cache_path_prefix' => 'glide_cache',
      'base_url' => NULL,
      'base_path' => 'img',
      'use_signed_urls' => false,
      'sign_key' => NULL,
      'driver' => 'gd',
      'add_params_to_svgs' => false,
      'default_params' => 
      array (
        'fm' => 'jpg',
        'q' => '80',
        'fit' => 'max',
      ),
      'lqip_default_params' => 
      array (
        'fm' => 'gif',
        'blur' => 100,
        'dpr' => 1,
      ),
      'social_default_params' => 
      array (
        'fm' => 'jpg',
        'w' => 900,
        'h' => 470,
        'fit' => 'crop',
      ),
      'cms_default_params' => 
      array (
        'q' => '60',
        'dpr' => '1',
      ),
      'presets' => 
      array (
      ),
    ),
    'dashboard' => 
    array (
      'modules' => 
      array (
      ),
      'analytics' => 
      array (
        'enabled' => false,
      ),
      'search_endpoint' => 'admin.search',
    ),
    'oauth' => 
    array (
      'providers' => 
      array (
        0 => 'google',
      ),
      'default_role' => 'VIEWONLY',
    ),
    'version' => '2.1.1',
  ),
  'twill-navigation' => 
  array (
    'simulations' => 
    array (
      'title' => 'Simulations',
      'can' => 'App\\Models\\SimulationTier\\Simulation',
      'module' => true,
    ),
    'caseContexts' => 
    array (
      'title' => 'Target Zones',
      'module' => true,
    ),
    'districts' => 
    array (
      'title' => 'Districts',
      'can' => '',
      'module' => true,
    ),
    'challenges' => 
    array (
      'title' => 'Tasks',
      'module' => true,
    ),
    'informationals' => 
    array (
      'title' => 'News',
      'module' => true,
    ),
    'affiliations' => 
    array (
      'title' => 'Affiliations',
      'module' => true,
    ),
    'users' => 
    array (
      'title' => 'Users',
      'module' => true,
    ),
  ),
  'view' => 
  array (
    'paths' => 
    array (
      0 => '/var/www/app/resources/views',
    ),
    'compiled' => '/var/www/app/storage/framework/views',
  ),
  'image' => 
  array (
    'driver' => 'gd',
  ),
  'cashier' => 
  array (
    'key' => NULL,
    'secret' => NULL,
    'path' => 'stripe',
    'webhook' => 
    array (
      'secret' => NULL,
      'tolerance' => 300,
    ),
    'model' => 'App\\User',
    'currency' => 'usd',
    'currency_locale' => 'en',
    'payment_notification' => NULL,
    'paper' => 'letter',
    'logger' => NULL,
  ),
  'passport' => 
  array (
    'private_key' => NULL,
    'public_key' => NULL,
    'client_uuids' => false,
  ),
  'analytics' => 
  array (
    'view_id' => NULL,
    'service_account_credentials_json' => '/var/www/app/storage/app/analytics/service-account-credentials.json',
    'cache_lifetime_in_minutes' => 1440,
    'cache' => 
    array (
      'store' => 'file',
    ),
  ),
  'permission' => 
  array (
    'models' => 
    array (
      'permission' => 'Spatie\\Permission\\Models\\Permission',
      'role' => 'Spatie\\Permission\\Models\\Role',
    ),
    'table_names' => 
    array (
      'roles' => 'roles',
      'permissions' => 'permissions',
      'model_has_permissions' => 'model_has_permissions',
      'model_has_roles' => 'model_has_roles',
      'role_has_permissions' => 'role_has_permissions',
    ),
    'column_names' => 
    array (
      'model_morph_key' => 'model_id',
    ),
    'display_permission_in_exception' => false,
    'display_role_in_exception' => false,
    'enable_wildcard_permission' => false,
    'cache' => 
    array (
      'expiration_time' => 
      DateInterval::__set_state(array(
         'y' => 0,
         'm' => 0,
         'd' => 0,
         'h' => 24,
         'i' => 0,
         's' => 0,
         'f' => 0.0,
         'weekday' => 0,
         'weekday_behavior' => 0,
         'first_last_day_of' => 0,
         'invert' => 0,
         'days' => false,
         'special_type' => 0,
         'special_amount' => 0,
         'have_weekday_relative' => 0,
         'have_special_relative' => 0,
      )),
      'key' => 'spatie.permission.cache',
      'model_key' => 'name',
      'store' => 'default',
    ),
  ),
  'jsonapi' => 
  array (
    'base_uri' => '',
  ),
  'postgis' => 
  array (
    'schema' => 'public',
  ),
  'tinker' => 
  array (
    'commands' => 
    array (
    ),
    'alias' => 
    array (
    ),
    'dont_alias' => 
    array (
      0 => 'App\\Nova',
    ),
  ),
);
